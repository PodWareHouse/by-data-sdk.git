# ByDataSDK

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ByDataSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ByDataSDK'
```

## Author

seedmorn_gf, yangfeng@liuliangwan.com

## License

ByDataSDK is available under the MIT license. See the LICENSE file for more info.
