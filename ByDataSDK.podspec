
Pod::Spec.new do |s|
  s.name             = 'ByDataSDK'
  s.version          = '0.1.2'
  s.summary          = '数据处理封装'

  s.description      = 'wawaduo数据处理封装'

  s.homepage         = 'https://gitee.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'seedmorn_gf' => 'yangfeng@liuliangwan.com' }
  s.source           = { :git => 'https://gitee.com/PodWareHouse/by-data-sdk.git', :tag => s.version.to_s }
  
  s.ios.deployment_target = '12.0'
  s.platform     = :ios, '12.0'
  s.swift_version = '5.0'
  s.static_framework = true

  s.source_files = 'ByDataSDK/Classes/**/*'
  
  s.resource_bundles = {
    'ByDataSDK' => ['ByDataSDK/Assets/**/*']
  }
  
  s..module_map = 'source/module.modulemap'
  

  s.frameworks = 'UIKit', 'AVFoundation'
  s.dependency 'Alamofire', '~> 5.6.2'
#  s.dependency 'HandyJSON', '~> 5.0.2'
  s.dependency 'Qiniu', '~> 8.4.1'
  s.dependency 'SwiftMQTT', '~> 3.0.1'
  s.dependency 'Kingfisher', '~> 7.3.2'
  
end
