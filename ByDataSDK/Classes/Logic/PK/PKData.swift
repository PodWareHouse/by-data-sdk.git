//
//  PKData.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2023/1/19.
//

import UIKit

public protocol PKDataDelegate: AnyObject {
    func PKData_RequestSuccess()
    func PKData_RequestFail(error: String)
    func PKData_ReloadData()
}

public class PKData: NSObject {
    
    public weak var delegate: PKDataDelegate?
    
    public var cards: [Int] = [0, 0, 0, 0]
    public var maxs: [Int] = [2, 3, 4, 5, 6, 8, 10, 15, 20, 0]
    public var buss: [Int] = [1, 2]
    public var prices: [Int] = []
    public var pushs: [Int] = []
    public var games: [Int] = []
    public var dolls: [DollModel] = []
    
    private var info: PKCreateInfoModel?
    
    public var card: Int = -1
    public var max: Int = -1
    public var bus: Int = -1
    public var price: Int = -1
    public var doll: Int = -1
    public var pwd: String?
    
    public override init() {
        super.init()
        
    }
    
    
}

public extension PKData {
    func dollModel() -> DollModel? {
        if (doll >= 0 && doll < dolls.count) {
            return dolls[doll]
        }
        return nil
    }
}

public extension PKData {
    func didSelectCard(row: Int) {
        switch cards[row] {
        case 1:
            self.card = row
            
            if (self.max < 0 && self.maxs.count > 0) {
                self.didSelectMax(row: 0)
            }
            
            if (self.bus < 0 && self.buss.count > 0) {
                self.didSelectBus(row: 0)
            } else {
                self.didSelectBus(row: self.bus)
            }
            
            self.delegate?.PKData_ReloadData()
        default:
            break
        }
    }
    
    func didSelectMax(row: Int) {
        self.max = row
        self.delegate?.PKData_ReloadData()
    }
    
    func didSelectBus(row: Int) {
        self.bus = row
        self.updatePrices(card: self.card, bus: row)
        self.delegate?.PKData_ReloadData()
    }
    
    func didSelectPrice(row: Int) {
        self.price = row
        self.updateDollList(bus: self.bus, price: row)
        self.delegate?.PKData_ReloadData()
    }
    
    func didSelectDoll(row: Int) {
        self.doll = row
        self.delegate?.PKData_ReloadData()
    }
    
    private func updatePrices(card: Int, bus: Int) {
        var area: [Int] = []
        switch card {
        case 0:
            area = self.info?.initialPrice ?? []
        case 1:
            area = self.info?.middlePrice ?? []
        case 2:
            area = self.info?.seniorPrice ?? []
        case 3:
            area = self.info?.highestPrice ?? []
        default:
            break
        }
        
        switch self.buss[bus] {
        case 1:
            self.prices = self.availableArray(all: self.pushs, area: area)
        case 2:
            self.prices = self.availableArray(all: self.games, area: area)
        default:
            self.prices = []
            break
        }
        
        if (self.price < 0) {
            if (self.prices.count > 0) {
                self.price = 0
            }
        } else {
            if (self.price >= self.prices.count) {
                self.price = 0
            }
        }
        self.updateDollList(bus: bus, price: self.price)
    }
    
    private func updateDollList(bus: Int, price: Int) {
        if (bus < 0 || price < 0) {
            self.dolls = []
            self.doll = -1
        } else {
            let bustype: Int = self.buss[bus]
            let pri: Int = self.prices[price]
            self.requestDollList(busType: bustype, roomid: nil, price: pri)
        }
        self.delegate?.PKData_ReloadData()
    }
    
    private func availableArray(all: [Int], area: [Int]) -> [Int] {
        
        let min: Int = area.first ?? 0
        let max: Int = area.last ?? 0
        var list: [Int] = []
        for a in all {
            if (a >= min && a <= max) {
                list.append(a)
            }
        }
        return list
    }
}

public extension PKData {
    func parseData(item: PKCreateInfoModel) {
        self.info = item
        
        self.cards = [(item.initial ?? 0), (item.middle ?? 0), (item.senior ?? 0), (item.highest ?? 0)]
        self.maxs = item.maxJoin ?? []
        self.buss = item.busType ?? []
        self.pushs = item.pushPrice ?? []
        self.games = item.gamePrice ?? []
        
        if (self.card < 0) {
            for i in 0..<self.cards.count {
                if (self.cards[i] > 0) {
                    self.didSelectCard(row: i)
                    break
                }
            }
        }
        
        self.delegate?.PKData_ReloadData()
    }
}

public extension PKData {
    func requestList() {
        NetworkPK().requestPkCreateRequire { item in
            self.delegate?.PKData_RequestSuccess()
            self.parseData(item: item)
        } fail: { error, result in
            self.delegate?.PKData_RequestFail(error: error)
        }
    }
    
    func requestDollList(busType: Int, roomid: Int?, price: Int?) {
        NetworkPK().requestPkBusList(busType: busType, roomid: roomid, price: price) { list in
            self.dolls = list
            if (self.doll < 0) {
                if (self.dolls.count > 0) {
                    self.doll = 0
                }
            } else {
                if (self.doll >= self.dolls.count) {
                    self.doll = 0
                }
            }
            self.delegate?.PKData_ReloadData()
        } fail: { error, result in
            self.dolls = []
            self.doll = -1
            self.delegate?.PKData_ReloadData()
            self.delegate?.PKData_RequestFail(error: error)
        }
    }
    
    func requestSubmit() {

    }
}

