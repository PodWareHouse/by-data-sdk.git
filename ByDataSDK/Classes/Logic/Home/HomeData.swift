//
//  HomeData.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2023/1/3.
//

import UIKit

public protocol HomeDataDelegate: AnyObject {
    func HomeData_list(banners: [BannerModel], records: [RecordModel])
    func HomeData_category(list: [DollClassifyModel])
}

public class HomeData: NSObject {

    public weak var delegate: HomeDataDelegate?
    
    public var banners: [BannerModel] = []
    public var records: [RecordModel] = []
    public var categorys: [DollClassifyModel] = []
    
    public override init() {
        super.init()
        self.requestHallList()
        self.requestCategory()
    }
    
    public func requestData() {
        self.requestHallList()
        self.requestCategory()
    }
}

// MARK：- 十、net
extension HomeData {
    
    /// 1、获取轮播图和消息列表
    public func requestHallList() {
        NetworkApp().requestBannerList { banners, records in
            self.banners = banners
            self.records = records
            self.delegate?.HomeData_list(banners: banners, records: records)
        } fail: { error, result in
            self.banners = []
            self.records = []
            self.delegate?.HomeData_list(banners: [], records: [])
        }
    }
    
    /// 2、获取分类列表
    public func requestCategory() {
        NetworkDoll().requestDollClassify { list in
            self.categorys = list
            self.delegate?.HomeData_category(list: list)
        } fail: { error, result in
            self.categorys = []
            self.delegate?.HomeData_category(list: [])
        }
    }
}


