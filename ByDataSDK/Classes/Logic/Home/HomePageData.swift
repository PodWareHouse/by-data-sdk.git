//
//  HomePageData.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2023/1/3.
//

import UIKit

public protocol HomePageDataDelegate: AnyObject {
    func HomePageData_list(list: [DollModel])
}

public class HomePageData: NSObject {
    
    public weak var delegate: HomePageDataDelegate?
    private var type: Int?
    
    public var list: [DollModel] = []
    
    public init(type: Int?) {
        super.init()
        self.type = type
    }
}

extension HomePageData {
    public func requestList(alert: Bool) {
        if (alert) {
            
        }
        NetworkDoll().requestDollList(type: type ?? 0) { list in
            self.list = list
            self.delegate?.HomePageData_list(list: list)
        } fail: { error, result in
            self.delegate?.HomePageData_list(list: [])
        }
    }
}
