//
//  JsonData.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2023/1/7.
//

import UIKit

public class JsonData: NSObject {
    
    private static let text: JsonModel? = Bundle.jsonModel(podName: MainData.podName, fileName: "text")
    private static let params: JsonModel? = Bundle.jsonModel(podName: MainData.podName, fileName: "params")
    private static let api: JsonModel? = Bundle.jsonModel(podName: MainData.podName, fileName: "api")
    private static let info: JsonModel? = Bundle.jsonModel(podName: MainData.podName, fileName: "info")
    
}

// MARK: 二、读取json dic数据
public extension JsonData {
    
    /// 1、读取text.json中的数据
    /// - Parameter key: 键值
    /// - Returns: 数据
    static func textDicJson(_ key: String) -> String {
        return self.readDicJson(item: text, key: key)
    }
    
    /// 2、读取params.json中的数据
    /// - Parameter key: 键值
    /// - Returns: 数据
    static func paramsDicJson(_ key: String) -> String {
        return self.readDicJson(item: params, key: key)
    }
    
    /// 3、读取api.json中的数据
    /// - Parameter key: 键值
    /// - Returns: 数据
    static func apiDicJson(_ key: String) -> String {
        return self.readDicJson(item: api, key: key)
    }

    /// 4、读取info.json中的数据
    /// - Parameter key: 键值
    /// - Returns: 数据
    static func infoDicJson(_ key: String) -> String {
        return self.readDicJson(item: info, key: key)
    }
}

fileprivate extension JsonData {
    
    /// 1、读取json中的list数据
    /// - Parameters:
    ///   - item: model
    ///   - row: 下标
    /// - Returns: 数据
    static func readJson(item: JsonModel?, row: Int) -> String {
        let list: [String] = item?.list ?? []
        if (row < list.count && row > 0) {
            let value: String = list[row]
            return value.removeKey(item?.key)
        }
        return ""
    }
    
    /// 2、读取json中的dic数据
    /// - Parameters:
    ///   - item: model
    ///   - key: 键值
    /// - Returns: 数据
    static func readDicJson(item: JsonModel?, key: String) -> String {
        let dic: [String: String]? = item?.dic
        if (dic != nil && dic!.keys.contains(key)) {
            let value: String = dic![key] ?? ""
            if (value.isEmpty) {
                print("readDicJson - key = \(key) , value ============= nil")
            }
            return value.removeKey(item?.key)
        }
        print("readDicJson - key = \(key) , dic ============= nil")
        return ""
    }
}
