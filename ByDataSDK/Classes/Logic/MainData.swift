//
//  MainData.swift
//  Alamofire
//
//  Created by seedmorn_gf on 2022/12/23.
//

import UIKit

public protocol MainDataDelegate: AnyObject {
    func MainDataDidReceiveTimeOut(_ msg: String?)
}

public class MainData: NSObject {
    
    public weak var delegate: MainDataDelegate?
    
    static public let instance = MainData()
    static let podName = "ByDataSDK"
    
    private let savekey: String = "MainData"
    private let musickey: String = "MusicKey"
    
    public var appinfo: AppInfoModel?
    
    public var model: LoginModel?
    public var coin: UserCoinModel?
    public var music: MusicModel = MusicModel()
    
    public var im: IMData = IMData()
    
    private override init() {
        super.init()
        
        let obj: String? = UserDefaults.standard.object(forKey: savekey) as? String
        if (obj != nil) {
            guard let data = obj!.data(using: String.Encoding.utf8, allowLossyConversion: false) else {
                return
            }
            self.model = data.toCodable()
            NetworkTool.instance.updateData(uid: model?.user?.uid ?? 0, loginKey: model?.loginKey ?? "")
        }
        
        let mu: String? = UserDefaults.standard.object(forKey: musickey) as? String
        if (mu != nil) {
            guard let data = mu!.data(using: String.Encoding.utf8, allowLossyConversion: false) else {
                return
            }
            self.music = data.toCodable() ?? MusicModel()
        } else {
            UserDefaults.standard.set(music.toJSONString(), forKey: musickey)
        }
    }
}

fileprivate extension MainData {
    
    /// 1、缓存登录数据
    func cacheSignInData(item: LoginModel) {
        UserDefaults.standard.set(item.toJSONString(), forKey: savekey)
    }
    
    /// 2、删除登录数据
    func deleteSignInData() {
        UserDefaults.standard.removeObject(forKey: savekey)
    }
    
    /// 3、缓存音频设置数据
    /// - Parameter item: 音频设置
    func cacheMusicData(item: MusicModel) {
        UserDefaults.standard.set(item.toJSONString(), forKey: musickey)
    }
}

// MARK: 一、登录注册
public extension MainData {
    
    /// 1、登录请求成功
    /// - Parameter item: 登录数据
    func parseSignIn(item: LoginModel, completion: @escaping () -> Void) {
        print("登录请求成功 - uid = \(item.user?.uid ?? -1)")
        self.model = item
        
        // 1、登录成功，更新请求头
        NetworkTool.instance.updateData(uid: item.user?.uid, loginKey: item.loginKey)
        
        // 2、缓存登录数据
        self.cacheSignInData(item: item)
        
        // 3、初始化 IM
        self.im.parseSignIn(item.connector)
        
        // 4、获取余额信息
        self.requestCoin { item in
            completion()
        } fail: { error, result in
            completion()
        }
    }
    
    /// 2、退出登录请求成功
    /// - Parameter completion: 回调
    func parseSignOut(completion: @escaping () -> Void) {
        print("退出登录请求成功")
        // 1、退出登录，更新请求头
        NetworkTool.instance.updateData(uid: 0, loginKey: "")
        
        // 2、删除缓存数据
        self.deleteSignInData()
        
        // 3、更新信息
        self.model = nil
        self.coin = nil
        
        // 4、退出IM
        self.im.parseSignOut()
        
        completion()
    }
    
    /// 3、缓存背景音乐设置
    /// - Parameter open: 设置
    func saveBgMusic(open: Bool) {
        music.bgmusic = open
        self.cacheMusicData(item: music)
    }
    
    /// 4、缓存视频音效设置
    /// - Parameter open: 设置
    func saveVideoMusic(open: Bool) {
        music.videomusic = open
        self.cacheMusicData(item: music)
    }
    
    /// 5、缓存弹幕设置
    /// - Parameter open: 设置
    func saveBarrage(open: Bool) {
        music.showbarrage = open
        self.cacheMusicData(item: music)
    }
    
    func saveLoginModel() {
        if (model != nil) {
            self.cacheSignInData(item: model!)
        }
    }
}

public extension MainData {
    
    /// 1、获取余额信息
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    func requestCoin(completion: @escaping (_ item: UserCoinModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        NetworkUser().requestUserCoin(completion: { item in
            self.coin = item
            completion(item)
        }, fail: fail)
    }
    
    func requestToLogout(completion: @escaping () -> Void) {
        let uid: Int? = self.model?.user?.uid
        if (uid != nil) {
            NetworkUser().requestUserLoginOut { item in
                MainData.instance.parseSignOut {
                    completion()
                }
            } fail: { error, result in
                completion()
            }
        } else {
            MainData.instance.parseSignOut {
                completion()
            }
        }
    }
}
