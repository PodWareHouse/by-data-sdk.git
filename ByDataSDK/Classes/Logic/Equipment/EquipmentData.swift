
import UIKit

public protocol EquipmentDataDelegate: AnyObject {
    func EquipmentDataDidReceiveMainTimerMethod(_ time: Int)
    func EquipmentDataToOther(item: DollModel?, tip: String?)
    func EquipmentDataNotMoney()
    func EquipmentDataShowError(error: String)
    func EquipmentDataShowTip(tip: String)
    func EquipmentDataUpdateState()
    func EquipmentDataApply(result: Bool)
    func EquipmentDataUpdateMoney()
    func EquipmentDataUpdateOptAction(_ opt: Int)
    func EquipmentDataWillPlayTimeOut()
    
    func EquipmentDataPushCoin()
    func EquipmentDataDidReceive(_ head: String?, _ name: String?, _ level: String?)
    func EquipmentDataChackBJ(_ list: [Int])
    func EquipmentDataBJTip(_ a: Int, _ tip: String, _ name: String)
    func EquipmentDataBJResult(_ busId: Int, _ tip: String?, _ name: String)
    func EquipmentDataBoxValue(_ isopen: Bool, _ pro: CGFloat)
    func EquipmentDataJPValue(_ item: PCJPModel)
    func EquipmentDataChatList(_ list: [RoomChatModel])
    func EquipmentDataWin(_ coin: Int)
    
    func EquipmentDataUpdateSeat(_ list: [GAllStatusModel])
}

public extension EquipmentDataDelegate {
    func EquipmentDataPushCoin() {}
    func EquipmentDataDidReceive(_ head: String?, _ name: String?, _ level: String?){}
    func EquipmentDataChackBJ(_ list: [Int]) {}
    func EquipmentDataBJTip(_ a: Int, _ tip: String, _ name: String) {}
    func EquipmentDataBJResult(_ busId: Int, _ tip: String?, _ name: String) {}
    func EquipmentDataBoxValue(_ isopen: Bool, _ pro: CGFloat) {}
    func EquipmentDataJPValue(_ item: PCJPModel) {}
    func EquipmentDataChatList(_ list: [RoomChatModel]) {}
    func EquipmentDataWin(_ coin: Int) {}
    
    func EquipmentDataUpdateSeat(_ list: [GAllStatusModel]) {}
}

public class EquipmentData: NSObject {

    public weak var delegate: EquipmentDataDelegate?
    
    private var model: DollModel
    
    private var timer: Timer?
    private var time: Int = 0
    private var autoTimer: Timer?
    
    private let pushNet: NetworkPush
    private let gameNet: NetworkGame
    
    public let uid: Int?
    
    // 1、设备数据
    private var type: Int?
    private var busId: Int
    private var optId: Int?
    public var playTime: Int = 0
    public var state: PushState = .none
    
    // 2、包机和pk
    public var isfree: Bool = false
    public var freeCoin: Int = 0
    public var freeTime: Int = 0
    public var freelist: [FreePlayModel]?
    public var showFreeItem: Bool {
        if (self.ispk || self.isfree || freelist == nil || freelist!.isEmpty) {
            return false
        }
        return true
    }

    public var ispk: Bool = false
    public var pkCoin: Int = 0
    public var pkTime: Int = 0
    public var pkmodel: PKRoomModel?
    public var showPkItem: Bool {
        if (self.pkmodel == nil) {
            return false
        }
        if (self.pkmodel!.maxJoin == nil || self.pkmodel!.maxJoin!.isEmpty) {
            return false
        }
        if (self.ispk || isfree) {
            return false
        }
        return true
    }
    
    // 3、其他数据
    public var autofire: Bool = false
    public var autolock: Bool = false
    public var incoming: Bool = false
    public var isshowjp: Bool = false
    public var iscreateui: Bool = false
    public var downSeats: [GSeatModel] = []
    public var upSeats: [GSeatModel] = []
    public var tips: String?
    
    public var money: Int {
        return MainData.instance.coin?.coin ?? 0
    }
    
    public init(item: DollModel, uid: Int?) {
        self.model = item
        self.uid = uid
        self.type = item.type
        self.busId = item.busId ?? 0
        self.pushNet = NetworkPush(uid: uid)
        self.gameNet = NetworkGame(uid: uid, type: item.type)
        
        super.init()
        
        // 1、开启主定时器
        self.initTimer()
        
        // 2、初始化座位
        self.initSeat(seatType: item.seatType, seatMap: item.seatMap)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("deinit - EquipmentData")
    }
}

// MARK: - 零、外部调用
extension EquipmentData {
    public func startCreateUI() {
        guard self.iscreateui == false else {
            return
        }
        self.iscreateui = true
        switch self.type {
        case 1,2,4:
            // 3、获取包机权益
            self.requestFreeList()
            // 4、获取pk权益
            self.requestHasPK()
        default:
            break
        }
        
        switch self.type {
        case 1:
            // 8、获取聊天列表
            self.roomChatList()
        default:
            break
        }
    }
    
    public func releaseData() {
        self.invalidateTimer()
        self.invalidateAutoTimer()
    }
    
    public func pauseTimer() {
        self.timer?.fireDate = Date.distantFuture
    }
    public func resumeTimer() {
        self.timer?.fireDate = Date.distantPast
    }
    
    public func startAutoTimer() {
        switch self.type {
        case 1,11:
            self.initAutoTimer(0.5)
        default:
            break
        }
    }
    
    public func stopAutoTimer() {
        self.invalidateAutoTimer()
    }
    
    public func quitEquipment(completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        switch self.type {
        case 1:
            self.requestPushFinish(completion: completion, fail: fail)
        default:
            self.requestGameFinish(completion: completion, fail: fail)
        }
    }
    
    public func applyEquipment(item: GSeatModel?) {
        switch self.type {
        case 1:
            self.requestPushApply()
        default:
            self.busId = item?.busId ?? self.model.busId ?? 0
            self.requestGameApply()
        }
    }
    
    public func optAction(opt: Int) {
        self.requestOpt(opt) {
            switch opt {
            case 12, 13:
                self.requestStatus()
            case 5:
                self.incoming = true
            case 17:
                self.autolock = true
            case 18:
                self.autolock = false
            default:
                break
            }
            self.delegate?.EquipmentDataUpdateOptAction(opt)
        } fail: { error in
            self.delegate?.EquipmentDataUpdateOptAction(-1)
        }
    }
    
    public func pushCoin() {
        switch self.type {
        case 1:
            self.requestPushCoin()
        default:
            self.optAction(opt: 8)
            break
        }
    }
    
    public func pushGameCoin(_ coin: Int,
                             completion: @escaping (_ item: GCoinModel) -> Void,
                             limitHander: @escaping (_ tip: String, _ coin: Int?) -> Void,
                             notMoneyHander: @escaping () -> Void,
                             fail: @escaping (_ error: String) -> Void) {
        self.requestGameCoin(coin, completion: completion, limitHander: limitHander, notMoneyHander: notMoneyHander, fail: fail)
    }
    
    public func startFreePlay(_ item: FreePlayModel, completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        switch self.type {
        case 1:
            self.requestStartFreePlay(item, completion: completion, fail: fail)
        default:
            self.requestGameFreePlay(item, completion: completion, fail: fail)
            break
        }
    }
    
    public func startPK(_ max: Int, _ pwd: String?, completion: @escaping (_ msg: String) -> Void, fail: @escaping (_ error: String) -> Void) {
        let cardType: Int = self.pkmodel?.cardType ?? 0
        self.requestStartPK(max, pwd, cardType: cardType, completion: completion, fail: fail)
    }
    
    public func startRepair() {
        self.requestRepair()
    }
    
    public func checkBJState() {
        self.requestBJState()
    }
    
    public func showBJTip(_ a: Int, _ name: String) {
        self.requestBJTip(a, name)
    }
    
    public func startBI(_ a: Int, _ name: String) {
        self.requestBJ(a, name)
    }
    
    public func appointmentState(tipsHander: @escaping (_ tip: String) -> Void,
                                 cancelHander: @escaping () -> Void,
                                 otherHander: @escaping (_ item: DollModel, _ tip: String, _ bid: Int) -> Void,
                                 fail: @escaping (_ error: String) -> Void) {
        self.requestAppointmentState(tipsHander: tipsHander, cancelHander: cancelHander, otherHander: otherHander, fail: fail)
    }
    
    public func appointment(completion: @escaping (_ tip: String) -> Void,
                            fail: @escaping (_ error: String) -> Void) {
        self.requestAppointment(completion: completion, fail: fail)
    }
    
    public func appointmentCancel(completion: @escaping () -> Void,
                                  fail: @escaping (_ error: String) -> Void) {
        self.requestAppointmentCancel(completion: completion, fail: fail)
    }
    
    public func startRain() {
        self.requestRain()
    }
    
    public func openBox() {
        self.requestOpenBox()
    }
    
    public func checkJP() {
        self.requestJP()
    }

    public func roomChatList() {
        self.requestRoomChatList()
    }
    
    public func roomChat(_ text: String, completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        self.requestRoomChat(text, completion: completion, fail: fail)
    }
}



// MARK: - 一、数据处理
extension EquipmentData {
    /// 1、主定时器方法
    @objc func mainTimerMethod() {
        if (self.time % 2 == 0) {
            
            // 1、获取设备状态
            self.requestStatus()
            
            // 2、获取座位状态
            switch self.type {
            case 0,1,6,9,11,13,14,15:
                // 不用获取座位状态
                break
            default:
                switch self.state {
                case .none, .free, .occupy:
                    self.requestSeatStatus()
                default:
                    break
                }
                break
            }
            
            // 5、结算中
            if (self.incoming) {
                self.requestGameIncome()
            }
            
            switch self.type {
            case 1:
                // 6、查询宝箱值
                self.requestBoxValue()
                
                // 7、jp
                if (self.isshowjp) {
                    self.checkJP()
                }
                switch self.state {
                case .play:
                    // 10、win
                    self.requestWin()
                default:
                    break
                }
            default:
                break
            }
        }
        
        if (self.time % 10 == 0) {
            // 2、获取座位状态
            switch self.type {
            case 0,1,6,9,11,13,14,15:
                // 不用获取座位状态
                break
            default:
                switch self.state {
                case .play:
                    self.requestSeatStatus()
                default:
                    break
                }
                break
            }
        }
        
        if (self.isfree) {
            self.freeTime -= 1
            if (self.freeTime < 0) {
                self.freeTime = 0
            }
        }
        
        if (self.ispk) {
            self.pkTime -= 1
            if (self.pkTime < 0) {
                self.pkTime = 0
            }
        }
        
        if (self.playTime > 0) {
            self.playTime -= 1
        }
        
        if (self.playTime == 8 && !self.incoming) {
            self.delegate?.EquipmentDataWillPlayTimeOut()
        }
        
        if ((self.playTime <= 0 || self.state == .reserve) && self.autofire) {
            switch self.type {
            case 1,11:
                self.stopAutoTimer()
            default:
                self.optAction(opt: 13)
            }
        }
        
        self.time += 1
        self.delegate?.EquipmentDataDidReceiveMainTimerMethod(self.time)
    }
    
    @objc func autoTimerMethod() {
        self.pushCoin()
    }
    
    /// 2、获取机器状态
    private func requestStatus() {
        switch self.type {
        case 1:
            self.requestPushStatus()
        default:
            self.requestGameStatus()
        }
    }
}

// MARK: - 二、内部方法调用
extension EquipmentData {
    /// 1、初始化主定时器
    private func initTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(mainTimerMethod), userInfo: nil, repeats: true)
        self.timer?.fireDate = Date.distantPast
    }
    
    private func initAutoTimer(_ ti: TimeInterval) {
        self.autofire = true
        self.autoTimer = Timer.scheduledTimer(timeInterval: ti, target: self, selector: #selector(autoTimerMethod), userInfo: nil, repeats: true)
        self.autoTimer?.fireDate = Date.distantPast
    }
    
    private func invalidateTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    private func invalidateAutoTimer() {
        self.autofire = false
        self.autoTimer?.invalidate()
        self.autoTimer = nil
    }
    

    
    
    /// 2、初始化座位信息
    /// - Parameters:
    ///   - seatType: 不同方向座位数量
    ///   - seatMap: 座位字典
    private func initSeat(seatType: String?, seatMap: [String: [String: Int]]?) {
        if (seatType == nil || seatMap == nil) {
            return
        }

        let arr: [String] = seatType!.components(separatedBy: "-")
        let all: Int = countSeatType(arr, 1)
        let down: Int = countSeatType(arr, 3)
        let right: Int = countSeatType(arr, 5)
        let up: Int = countSeatType(arr, 7)

        self.downSeats.removeAll()
        self.upSeats.removeAll()

        for i in 0..<all {
            let seat: String = String(i+1)
            let dic: [String: Int]? = seatMap![seat]
            var mo: GSeatModel?
            if (dic != nil) {
                mo = dic?.toData()?.toCodable()
            }
            if (mo == nil) {
                mo = GSeatModel(seat: Int(seat))
            }
            
            if (i < down) {
                self.downSeats.append(mo!)
            } else if (i < down+right) {
                
            } else if (i < down+right+up) {
                self.upSeats.append(mo!)
            }
        }
        
        self.delegate?.EquipmentDataUpdateSeat([])
    }
    
    private func countSeatType(_ arr: [String], _ index: Int) -> Int {
        var a: Int = 0
        if (arr.count > index) {
            a = Int(arr[index]) ?? 0
        }
        return a
    }
    
    private func updateSeate(list: [GAllStatusModel], array: [GSeatModel]) -> [GSeatModel] {
        var newArr: [GSeatModel] = []
        for i in 0..<array.count {
            var mo = array[i]
            for item in list {
                switch item.seat {
                case mo.seat:
                    mo.state = item
                    break
                default:
                    break
                }
            }
            newArr.append(mo)
        }
        return newArr
    }
    
//    private func parseStateAutoApply(_ list: [GSeatModel]) {
//        // 1、如果有已经上机的，自动申请上机
//        for item in list {
//            let status: Int = item.state?.status ?? 0
//            if (status == 1) {
//                if (self.state != .play) {
//                    let myuid: Int = self.uid ?? 0
//                    let itemuid: Int = item.state?.uid ?? 0
//                    if (itemuid > 0 && itemuid == myuid) {
//                        print("如果有已经上机的，自动申请上机, state = \(self.state)")
//                        self.applyEquipment(item: item)
//                        /*
//                         "busId": 221,
//                         "uid": 20508777,
//                         "optId": 0
//                         */
//                    }
//                }
//            }
//        }
//    }
}

// MARK: - 三、push and hallo
extension EquipmentData {
    /// 1、玩法权益
    func requestFreeList() {
        NetworkRoom().requestFreePlayPermission(bid: self.busId) { list in
            self.freelist = list
            self.delegate?.EquipmentDataUpdateState()
        } fail: { error, result in
            self.freelist = nil
        }
    }
    
    /// 2、开始包机
    /// - Parameter item: item
    func requestStartFreePlay(_ item: FreePlayModel, completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        NetworkRoom().requestFreePlay(bid: self.busId, perid: item.id) {
            self.requestFreeList()
            self.requestStatus()
            completion()
        } fail: { error, result in
            fail(error)
        }
    }
    
    /// 3、PK权益
    func requestHasPK() {
        NetworkPK().requestPkBusCard(busid: self.busId) { item in
            self.pkmodel = item
            self.delegate?.EquipmentDataUpdateState()
        } fail: { error, result in
            self.pkmodel = nil
        }
    }
    
    /// 4、房间内发起赛事
    func requestStartPK(_ max: Int, _ pwd: String?, cardType: Int, completion: @escaping (_ msg: String) -> Void, fail: @escaping (_ error: String) -> Void) {
        NetworkPK().requestPkCreate(busid: self.busId, cardType: cardType, envType: 2, maxJoin: max, pwd: pwd) { item, msg in
            completion(msg)
        } fail: { error, result in
            fail(error)
        }
    }
    
    /// 5、故障
    func requestRepair() {
        NetworkRoom().requestRepairReport(bid: self.busId) { tip in
            self.delegate?.EquipmentDataShowTip(tip: tip ?? "")
        }
    }
    
    /// 6、查询霸机权限
    func requestBJState() {
        NetworkRoom().requestSeatPermission(bid: self.busId) { list in
            self.delegate?.EquipmentDataChackBJ(list)
        } fail: { error, result in
            self.delegate?.EquipmentDataShowError(error: error)
        }
    }
    
    /// 7、霸机提示信息
    func requestBJTip(_ a: Int, _ name: String) {
        NetworkRoom().requestSeatPreset(bid: self.busId, seatType: a) { tip in
            self.delegate?.EquipmentDataBJTip(a, tip, name)
        } fail: { error, result in
            self.delegate?.EquipmentDataShowError(error: error)
        }
    }
    
    /// 9、霸机
    func requestBJ(_ a: Int, _ name: String) {
        NetworkRoom().requestSeatSeat(bid: self.busId, seatType: a) { item in
            let busid: Int = item.busId ?? 0
            if (busid > 0) {
                self.delegate?.EquipmentDataBJResult( busid, item.msg, name)
            } else {
                self.delegate?.EquipmentDataShowTip(tip: item.msg ?? "")
                self.requestStatus()
            }
        } fail: { error, result in
            self.delegate?.EquipmentDataShowError(error: error)
        }
    }
    
    // 10、预约信息
    func requestAppointmentState(tipsHander: @escaping (_ tip: String) -> Void,
                                 cancelHander: @escaping () -> Void,
                                 otherHander: @escaping (_ item: DollModel, _ tip: String, _ bid: Int) -> Void,
                                 fail: @escaping (_ error: String) -> Void) {
        NetworkDoll().requestDollPresubscribe(bid: self.busId, tipsHander: tipsHander, cancelHander: cancelHander, otherHander: otherHander) { error, result  in
            fail(error)
        }
    }
    
    // 11、预约
    func requestAppointment(completion: @escaping (_ tip: String) -> Void,
                            fail: @escaping (_ error: String) -> Void) {
        NetworkDoll().requestDollSubscribe(bid: self.busId, completion: completion) { error, result in
            fail(error)
        }
    }
    
    // 12、取消预约
    func requestAppointmentCancel(completion: @escaping () -> Void,
                                  fail: @escaping (_ error: String) -> Void) {
        NetworkDoll().requestDollCancelSubscribe(bid: self.busId, completion: completion) { error, result in
            fail(error)
        }
    }
    
    // 13、房间聊天记录
    func requestRoomChatList() {
        NetworkRoom().requestRoomGetChat(bid: self.busId) { list in
            self.delegate?.EquipmentDataChatList(list)
        } fail: { error, result in
            self.delegate?.EquipmentDataChatList([])
        }
    }
    
    // 14、聊天
    func requestRoomChat(_ text: String, completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        NetworkRoom().requestRoomDoChat(bid: self.busId, content: text) { item in
            completion()
        } fail: { error, result in
            fail(error)
        }
    }
}

// MARK: - 四、push
extension EquipmentData {
    
    /// 1、申请上机
    func requestPushApply() {
        self.pushNet.requestPushApply(bid: self.busId, oid: self.optId) { item in
            
            self.optId = item.optId
            self.playTime = item.playTime ?? 0
        
            switch item.isFreePlay {
            case 1:
                self.isfree = true
                self.freeTime = item.freePlayCountDown ?? 0
                // 1、请求余额信息，用户获取freeCoin
                self.requestUserCoin()
            default:
                self.isfree = false
                self.freeCoin = 0
                self.freeTime = 0
            }
            self.requestStatus()
            self.delegate?.EquipmentDataApply(result: true)
            
        } otherHander: { item, tip in
            self.delegate?.EquipmentDataApply(result: false)
            self.delegate?.EquipmentDataToOther(item: item, tip: tip)
        } notMoneyHander: {
            self.delegate?.EquipmentDataApply(result: false)
            self.delegate?.EquipmentDataNotMoney()
        } fail: { error, result in
            self.delegate?.EquipmentDataApply(result: false)
            self.delegate?.EquipmentDataShowError(error: error)
        }
    }
    
    // 2、投币
    func requestPushCoin() {
        self.pushNet.requestPushPush(bid: self.busId, oid: self.optId) { item in
            self.optId = item.optId
            self.playTime = item.playTime ?? 0
            
            MainData.instance.coin?.coin = item.data?.coin
            self.freeCoin = item.data?.freeCoin ?? 0
            
            self.delegate?.EquipmentDataPushCoin()
            
        } notMoneyHander: {
            self.delegate?.EquipmentDataNotMoney()
        } applyHander: {
            self.requestPushApply()
        } fail: { error, result in
            
        }
    }
    
    // 3、摆动
    func requestRain() {
        self.pushNet.requestPushOperate(bid: self.busId, oid: self.optId)
    }
    
    // 4、推币机主动下机
    func requestPushFinish(completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        self.pushNet.requestPushFinish(bid: self.busId, oid: self.optId) {
            completion()
        } fail: { error in
            fail(error)
        }
    }
    
    // 5、获取用户余额信息
    func requestUserCoin() {
        self.pushNet.requestPushQueryAccount(bid: self.busId) { item in
            MainData.instance.coin?.coin = item.coin
            self.freeCoin = item.freeCoin ?? 0
            self.delegate?.EquipmentDataUpdateMoney()
        }
    }
    
    // 6、开宝箱
    func requestOpenBox() {
        self.pushNet.requestPushOpenBox(bid: self.busId) { tip in
            self.delegate?.EquipmentDataShowTip(tip: tip ?? "")
        } fail: { error, result in
            self.delegate?.EquipmentDataShowError(error: error)
        }
    }
    
    // 7、查询JP
    func requestJP() {
        self.pushNet.requestPushQueryJP(bid: self.busId) { item in
            self.delegate?.EquipmentDataJPValue(item)
        } fail: { error, result in
            
        }
    }
    
    // 8、宝箱值
    func requestBoxValue() {
        self.pushNet.requestPushQueryBoxValue(bid: self.busId) { item in
            let isopen: Int = item.isOpen ?? 0
            let canopen: Bool = isopen == 1 ? true: false
            let total: Int = item.total ?? 1
            let value: Int = item.value ?? 0
        
            if (total == 0) {
                self.delegate?.EquipmentDataBoxValue(canopen, 0)
            } else {
                self.delegate?.EquipmentDataBoxValue(canopen, CGFloat(value) / CGFloat(total))
            }
        } fail: { error, result in
            
        }
    }
    
    // 9、获取机器状态
    func requestPushStatus() {
        self.pushNet.requestPushStatus(busId: self.busId, optId: self.optId) { item, state in
            
            // 1、设备数据
            self.state = state
            self.optId = item.data?.optId
            self.playTime = item.data?.gameTime ?? 0
            
            // 2、包机和pk
            switch item.isFreePlay {
            case 1:
                self.isfree = true
                self.freeTime = item.cardCountDown ?? 0
            default:
                self.isfree = false
                self.freeCoin = 0
                self.freeTime = 0
            }
            switch item.usePKCard {
            case 1:
                self.ispk = true
                self.pkCoin = item.pkCoin ?? 0
                self.pkTime = item.pkCountDown ?? 0
            default:
                self.ispk = false
            }
            
            // 3、其他数据
            self.tips = item.tips
            
            // 4、更新状态
            self.delegate?.EquipmentDataUpdateState()
            self.delegate?.EquipmentDataDidReceive(item.playerHeader, item.playerName, item.playerVIP)
            
        } applyHander: {
            self.requestPushApply()
        }
    }
    
    // 10、
    func requestWin() {
        self.pushNet.requestPushOutRecord(bid: self.busId, oid: self.optId) { userCoin, outCoin in
            MainData.instance.coin?.coin = userCoin
            self.delegate?.EquipmentDataUpdateMoney()
            if (outCoin > 0) {
                self.delegate?.EquipmentDataWin(outCoin)
            }
        }
    }
    
}


// MARK: - 五、hallo
extension EquipmentData {
    
    // 1、申请上机
    func requestGameApply() {
        self.gameNet.requestGameApply(bid: self.busId, oid: self.optId) { item in
            
            self.optId = item.optId
            self.playTime = item.playTime ?? 0
            switch item.isFreePlay {
            case 1:
                self.isfree = true
                self.freeTime = item.freePlayCountDown ?? 0
                // 1、请求余额信息，用户获取freeCoin
                self.requestUserCoin()
            default:
                self.isfree = false
                self.freeCoin = 0
                self.freeTime = 0
            }
            self.requestStatus()
            self.delegate?.EquipmentDataApply(result: true)
            
        } otherHander: { item, tip in
            self.delegate?.EquipmentDataApply(result: false)
            self.delegate?.EquipmentDataToOther(item: item, tip: tip)
        } notMoneyHander: {
            self.delegate?.EquipmentDataApply(result: false)
            self.delegate?.EquipmentDataNotMoney()
        } fail: { error, result in
            self.delegate?.EquipmentDataApply(result: false)
            self.delegate?.EquipmentDataShowError(error: error)
        }
    }
    
    // 2、使用包机卡
    func requestGameFreePlay(_ item: FreePlayModel, completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        self.gameNet.requestGameFreePlay(bid: self.busId, oid: self.optId, pid: item.id, completion: completion) { error, result in
            fail(error)
        }
    }
    
    // 3、
    func requestOpt(_ opt: Int, comple: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        self.gameNet.requestGameOperate(bid: self.busId, oid: self.optId, opt: opt) {
            comple()
        } notMoneyHander: {
            self.delegate?.EquipmentDataNotMoney()
            fail("")
        } fail: { error in
            fail(error)
        }
    }
    
    // 4、投币
    func requestGameCoin(_ coin: Int,
                         completion: @escaping (_ item: GCoinModel) -> Void,
                         limitHander: @escaping (_ tip: String, _ coin: Int?) -> Void,
                         notMoneyHander: @escaping () -> Void,
                         fail: @escaping (_ error: String) -> Void) {
        self.gameNet.requestGameCoin(bid: self.busId, oid: self.optId, coin: coin) { item in
            self.playTime = item.playTime ?? 0
            self.requestUserCoin()
            self.requestStatus()
            completion(item)
        } limitHander: { tip, coin in
            limitHander(tip, coin)
        } notMoneyHander: {
            self.delegate?.EquipmentDataNotMoney()
            notMoneyHander()
        } fail: { error, result in
            fail(error)
        }
    }
    
    // 5、机器状态
    func requestGameStatus() {
        self.gameNet.requestGameStatus(bid: self.busId, oid: self.optId) { item, state, head, name, level in
            self.state = state
            self.busId = item.busId ?? 0
            self.optId = item.optId
            self.playTime = item.timeRemain ?? 0
            
            // 1、设置自动发炮状态
            switch self.type {
            case 1, 11:
                // 返回使用者信息
                self.delegate?.EquipmentDataDidReceive(head, name, level)
                break
            default:
                switch item.autoFire {
                case 1:
                    self.autofire = true
                default:
                    self.autofire = false
                }
                break
            }
            
            // 2、包机和pk
            switch item.freePlay {
            case 1:
                self.isfree = true
                self.freeCoin = item.freeCoin ?? 0
                self.freeTime = item.freePlayCountDown ?? 0
            default:
                self.isfree = false
                self.freeCoin = 0
                self.freeTime = 0
            }
            switch item.usePKCard {
            case 1:
                self.ispk = true
                self.pkCoin = item.pkCoin ?? 0
                self.pkTime = item.pkCountDown ?? 0
            default:
                self.ispk = false
            }
            //
//            self.parseStateAutoApply(self.upSeats+self.downSeats)
            // 3、更新状态
            self.delegate?.EquipmentDataUpdateState()
   
            
        }
    }
    
    // 6、所有位置状态
    func requestSeatStatus() {
        self.gameNet.requestGameAllStatus(bid: self.busId, oid: self.optId) { list in
            
            self.upSeats = self.updateSeate(list: list, array: self.upSeats)
            self.downSeats = self.updateSeate(list: list, array: self.downSeats)
            
            self.delegate?.EquipmentDataUpdateSeat(list)
        }
    }
    
    // 7、下机
    func requestGameFinish(completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        self.gameNet.requestGameExit(bid: self.busId, oid: self.optId, completion: completion) { error, result in
            fail(error)
        }
    }
    
    // 8、结算查询
    func requestGameIncome() {
        self.gameNet.requestGameIncome(bid: self.busId, oid: self.optId) {
            self.incoming = false
            self.delegate?.EquipmentDataUpdateOptAction(5)
            self.requestUserCoin()
        } fail: { error, result in
            
        }
    }
}

