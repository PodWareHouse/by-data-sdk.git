//
//  IMData.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2023/1/3.
//

import UIKit
import SwiftMQTT

enum IMState {
    case login  //  已登录
    case logout //  已登出
    case loging //  连接中
    case waitReconnect //  等待重新连接
}

public protocol IMDataDelegate: AnyObject {
    func IMDataDidReceiveSysMessage(indexPath: IndexPath, item: CmsReceiveModel)
    func IMDataDidReceiveCustomerMessage()
    func IMDataUpdateCustomerUnreadCount(count: Int)
    
    func IMDataDidReceiveRoomMsg(_ text: String?)
    func IMDataDidReceiveRoomSysMsg(_ item: CmsReceiveModel)
    func IMDataDidReceiveRoomChat(_ item: CmsReceiveModel)
}

public extension IMDataDelegate {
    func IMDataDidReceiveSysMessage(indexPath: IndexPath, item: CmsReceiveModel) {}
    func IMDataDidReceiveCustomerMessage() {}
    func IMDataUpdateCustomerUnreadCount(count: Int) {}
    
    func IMDataDidReceiveRoomMsg(_ text: String?) {}
    func IMDataDidReceiveRoomSysMsg(_ item: CmsReceiveModel) {}
    func IMDataDidReceiveRoomChat(_ item: CmsReceiveModel) {}
}

public class IMData: NSObject {

    public weak var delegate: IMDataDelegate?
    
    private var session : MQTTSession?
    private var state: IMState = .logout
    
    private var connect: ConnectorModel?
    
    public var cms: CmsModel?
    public var customer: CmsFriendsModel?
    private var bid: Int?
    
    public var systems: [CmsReceiveModel] = []
    
    public var unread: Int {
        var count: Int = 0
        for item in systems {
            count += (item.unread ?? 0)
        }
        return count
    }
    
    override init() {
        
    }
    
    /// 1、登录
    func parseSignIn(_ item: ConnectorModel?) {
        guard item != nil else {
            self.state = .logout
            return
        }
        self.connect = item
        // 1、注册/获取IM用户信息
        self.requestIMInfo(item!)
    }
    
    /// 2、退出
    func parseSignOut() {
        print("MQTT - 退出")
        // 1、取消订阅
        let topics: [String] = [topicCustomer, topicSys, topicBroadcast]
        self.session?.unSubscribe(from: topics, completion: nil)
        
        // 2、断开连接
        self.session?.disconnect()
        self.session?.delegate = nil
        
        // 3、清除信息
        self.cms = nil
        self.connect = nil
        self.bid = nil
        self.state = .logout
    }
    
    /// 3、跳转推币房间
    /// - Parameter bid: 机器id
    public func initPushRoom(bid: Int?) {
        self.bid = bid
        let topics: [String: MQTTQoS] = [topicRoomSys: .atMostOnce,
                                         topicRoomChat: .atMostOnce,
                                         topicRoomSysMsg: .atMostOnce]
        self.session?.subscribe(to: topics, completion: nil)
    }
    
    /// 4、退出推币房间
    public func deinitPushRoom() {
        self.bid = nil
        let topics: [String] = [topicRoomSys,topicRoomChat,topicRoomSysMsg]
        self.session?.unSubscribe(from: topics, completion: nil)
    }
    
    /// 5、跳转游戏房间
    /// - Parameter bid: 机器id
    public func initHalloRoom(bid: Int?) {
        self.bid = bid
        let topics: [String: MQTTQoS] = [topicRoomSysMsg: .atMostOnce]
        self.session?.subscribe(to: topics, completion: nil)
    }
    
    /// 6、退出游戏房间
    public func deinitHalloRoom() {
        self.bid = nil
        let topics: [String] = [topicRoomSysMsg]
        self.session?.unSubscribe(from: topics, completion: nil)
    }
    
    /// 7、更新数据已读
    public func updateSystemDataIsRead(item: CmsReceiveModel) {
        for i in 0..<self.systems.count {
            var mo = self.systems[i]
            if (CmsReceiveModel.same(mo, item)) {
                mo.setisread()
                self.systems[i] = mo
                self.saveSystemData()
            }
        }
    }
    
    public func deleteSystemData(item: CmsReceiveModel) {
        for i in 0..<self.systems.count {
            let mo = self.systems[i]
            if (CmsReceiveModel.same(mo, item)) {
                self.systems.remove(at: i)
                break
            }
        }
    }
}

extension IMData {
    
    /// 1、连接IM
    /// - Parameter item: 登录信息
    func connectSocket(_ item: ConnectorModel) {
        guard self.state != .loging else {
            return
        }
        print("MQTT - 连接中。。。")
        // 1、获取缓存的系统消息
        self.readReceiveSystemData()
        
        self.state = .loging
        session = MQTTSession(host: item.server?.host ?? "",
                              port: UInt16(item.server?.port ?? 0),
                              clientID: item.clientId ?? "",
                              cleanSession: true, keepAlive: 60,
                              connectionTimeout: 5.0,
                              useSSL: false)
        session?.username = item.userName
        session?.password = item.password
        session?.delegate = self
        session?.connect(completion: { error in
            switch error {
            case .none:
                print("MQTT - 连接成功")
                self.connectedSuccess()
            default:
                print("MQTT - 连接失败 = \(error)")
                self.state = .logout
                self.waitReconnect(item)
                break
            }
        })
    }
    
    /// 2、IM连接错误
    /// - Parameter item: 登录信息
    private func waitReconnect(_ item: ConnectorModel) {
        guard self.state != .waitReconnect else {
            return
        }
        print("MQTT - 连接错误, 2s后重新连接")
        self.state = .waitReconnect
        DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
            self.connectSocket(item)
        }
    }
    
    /// 3、IM连接成功
    private func connectedSuccess() {
        self.state = .login
        
        let topics: [String: MQTTQoS] = [topicCustomer: .atMostOnce,
                                         topicSys: .atMostOnce,
                                         topicBroadcast: .atMostOnce]
        session?.subscribe(to: topics, completion: nil)
        
        if (self.systems.isEmpty) {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                self.requestIMWelcome()
            }
        }
    }
}

extension IMData {
    static public func isWelfare(text: String?) -> Bool {
        guard text != nil else {
            return false
        }
        if (text!.starts(with: "other_20".text())) {
            return true
        }
        return false
    }
    
    static public func welfareCode(text: String?) -> String {
        guard text != nil else {
            return ""
        }
        if (text!.starts(with: "other_20".text())) {
            let arr = text!.components(separatedBy: " ")
            if (arr.count >= 2) {
                return arr.last!
            }
        }
        return ""
    }
}

extension IMData {
    
    /// 1、接收系统消息
    /// - Parameter item: 消息数据
    private func parseReceiveSystemData(item: CmsReceiveModel?) {
        guard var mo: CmsReceiveModel = item else {
            return
        }
        mo.setUnread()
        self.systems.append(mo)
        let indexpath: IndexPath = IndexPath(row: self.systems.count-1, section: 0)
        self.delegate?.IMDataDidReceiveSysMessage(indexPath: indexpath, item: mo)
        // 缓存系统消息
        self.saveSystemData()
    }
    
    private func saveSystemData() {
        let list: [[AnyHashable: Any]]? = self.systems.toArray()
        guard list != nil else {
            return
        }
        UserDefaults.standard.set(list!, forKey: systemKey)
    }
    
    /// 2、获取缓存的系统消息
    private func readReceiveSystemData() {
        let list: [[AnyHashable: Any]]? = UserDefaults.standard.object(forKey: systemKey) as? [[AnyHashable: Any]]
        guard list != nil else {
            return
        }
    
        let data: Data? = list!.toData()
        guard data != nil else {
            return
        }
        let arr: [CmsReceiveModel]? = data!.toCodable()
        print("获取缓存的系统消息 - systemKey = \(systemKey), arr = \(arr?.count ?? -1)")
        self.systems = arr ?? []
    }
}

extension IMData: MQTTSessionDelegate {
    public func mqttDidReceive(message: MQTTMessage, from session: MQTTSession) {
        let mo: DataCodable<CmsReceiveModel>? = message.payload.toCodable()
        guard mo?.data != nil else {
            print("mo.data == nil - \(message.stringRepresentation ?? "nil")")
            return
        }
        
        switch mo!.data?.subType {
        case 404, 504:
            MainData.instance.delegate?.MainDataDidReceiveTimeOut(mo?.data?.content)
            return
        default:
            break
        }
        
        switch message.topic {
        case topicCustomer:
            self.delegate?.IMDataDidReceiveCustomerMessage()
            self.requestCustomer { } fail: { error in }
        case topicSys:
            self.parseReceiveSystemData(item: mo?.data)
        case topicBroadcast:
            self.parseReceiveSystemData(item: mo?.data)
        case topicRoomSys:
            self.delegate?.IMDataDidReceiveRoomMsg(mo?.data?.content)
        case topicRoomChat:
            self.delegate?.IMDataDidReceiveRoomChat(mo!.data!)
        case topicRoomSysMsg:
            self.parseReceiveSystemData(item: mo?.data)
            self.delegate?.IMDataDidReceiveRoomSysMsg(mo!.data!)
        default:
            break
        }
    }
    
    public func mqttDidDisconnect(session: MQTTSession, error: MQTTSessionError) {
        switch error {
        case .none:
            print("mqttDidDisconnect 1 - Successfully disconnected from MQTT broker")
            
        default:
            print("mqttDidDisconnect 2 - \(error.description)")
            switch self.state {
            case .login, .loging:
                self.state = .logout
            default:
                break
            }
            if (self.connect != nil) {
                self.waitReconnect(self.connect!)
            }
            break
        }
    }
    
    public func mqttDidAcknowledgePing(from session: MQTTSession) {
        //print("mqttDidAcknowledgePing - ")
    }
}

extension IMData {
    private func requestIMInfo(_ mo: ConnectorModel) {
        NetworkCms().requestCmsUserApp { item in
            self.cms = item
            self.requestCustomer { } fail: { error in }
            self.connectSocket(mo)
        } fail: { error, result in
            DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                self.requestIMInfo(mo)
            }
        }
    }
    
    private func requestIMWelcome() {
        NetworkCms().requestMsgWelcome { item in
            
        } fail: { error, result in
            
        }
    }
    
    public func requestCleanUnread() {
        let tid: Int = self.customer?.user?.gameUid ?? 0
        NetworkCms().requestCmsIsRead(tid: tid) { item in
            self.requestCustomer { } fail: { error in }
        } fail: { error, result in }
    }
    
    public func requestCustomer(completion: @escaping () -> Void, fail: @escaping (_ error: String) -> Void) {
        NetworkCms().requestCmsFriendList { list in
            print("im list = \(list.count)")
            for item in list {
                print("userType = \(item.user?.userType ?? -1)")
                switch item.user?.userType {
                case 2:
                    print("item = \(item)")
                    self.customer = item
                    self.delegate?.IMDataUpdateCustomerUnreadCount(count: item.unMsgCount ?? 0)
                    break
                default:
                    break
                }
            }
            completion()
        } fail: { error, result in
            print("im list error = \(error)")
            fail(error)
        }
    }
}

extension IMData {
    private var systemKey: String {
        return "SystemKey-\(self.cms?.gameUid ?? 0)"
    }
    
    private var topicCustomer: String {
        var str = "im_10".params()
        str += "im_20".params() + "/"
        str += "im_30".params() + "/"
        str += "\(self.cms?.gameUid ?? 0)" + "/"
        str += "im_40".params()
        return str
    }
    
    private var topicSys: String {
        var str = "im_10".params()
        str += "im_20".params() + "/"
        str += "im_31".params() + "/"
        str += "\(self.cms?.gameUid ?? 0)" + "/"
        str += "im_40".params()
        return str
    }

    private var topicBroadcast: String {
        var str = "im_10".params()
        str += "im_21".params() + "/"
        str += "im_31".params() + "/"
        str += "im_40".params()
        return str
    }
    
    private var topicRoomSys: String {
        var str = "im_10".params()
        str += "im_22".params() + "/"
        str += "im_31".params() + "/"
        str += "im_40".params()
        return str
    }
    
    private var topicRoomChat: String {
        var str = "im_10".params()
        str += "im_22".params() + "/"
        str += "im_31".params() + "/"
        str += "\(self.bid ?? 0)" + "/"
        str += "im_40".params()
        return str
    }
    
    private var topicRoomSysMsg: String {
        var str = "im_10".params()
        str += "im_22".params() + "/"
        str += "im_31".params() + "/"
        str += "\(self.bid ?? 0)" + "/"
        str += "\(self.cms?.gameUid ?? 0)" + "/"
        str += "im_40".params()
        return str
    }
    
}
