//
//  MicrophoneManager.swift
//  WaWaDuo
//
//  Created by Byron on 2023/2/24.
//

import UIKit
import AVFAudio
import AVFoundation

public class MicrophoneManager: NSObject {
    
    public static func authPermission(_ response: @escaping (Bool) -> Void) {
        let state = AVAudioSession.sharedInstance().recordPermission
        switch state {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission(response)
        case .denied:
            response(false)
        default:
            response(true)
        }
    }
    
    public static var isauthorized: Bool {
        let state = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        switch state {
        case .authorized:
            return true
        default:
            return false
        }
    }
    
}
