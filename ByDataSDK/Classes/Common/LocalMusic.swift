//
//  LocalMusic.swift
//  Baby
//
//  Created by seedmorn_gf on 2023/2/2.
//

import UIKit
import AVFoundation

public class LocalMusic: NSObject {
    
    private var LocalMusic: AVAudioPlayer?
    
    public override init() {
        super.init()
    }
    
    deinit {
        self.releasePlayer()
    }
}

public extension LocalMusic {
    
    /// 1、初始化音乐播放器
    func initPlayer() {
        guard let path: String =  Bundle.musicPath(podName: MainData.podName, fileName: "music") else {
            print("music path == nil")
            return
        }
        do {
            self.LocalMusic = try AVAudioPlayer.init(contentsOf: URL.init(fileURLWithPath: path))
            self.LocalMusic?.numberOfLoops = -1
            self.LocalMusic?.prepareToPlay()
            self.LocalMusic?.pause()
        } catch _ {
        }
    }
    
    /// 2、释放播放器
    func releasePlayer() {
        self.LocalMusic?.stop()
        self.LocalMusic = nil
    }
    
    /// 3、暂停/继续播放器
    func mutePlayer(_ mute: Bool) {
        if (mute) {
            print("暂停背景音乐")
            self.LocalMusic?.pause()
        } else {
            print("播放背景音乐")
            self.LocalMusic?.play()
        }
    }
}
