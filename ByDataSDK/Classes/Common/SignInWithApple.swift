import UIKit
import AuthenticationServices

public protocol SignInWithAppleDelegate: AnyObject {
    func SignInWithAppleFail(error: String?)
    func SignInWithAppleSuccess(userIdentifier: String)
}

public class SignInWithApple: NSObject {

    public weak var delegate: SignInWithAppleDelegate?
    
    public override init() {
        super.init()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 1、登录事件处理
    public func handleAuthorizationAppleIDButtonPress() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
            
        } else {
            self.delegate?.SignInWithAppleFail(error: nil)
        }
    }
    
    /// 2、校验登录状态
    public func verifyingAuthorizationState(userIdentifier: String) {
        // 保存的  ASAuthorizationCredential.user 信息
        let userIdentifier = "appleIDCredential.user"
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            appleIDProvider.getCredentialState(forUserID: userIdentifier) { (credentialState, error) in
                switch credentialState {
                case .authorized:
                    print("The Apple ID credential is valid.")
                    break // The Apple ID credential is valid.
                case .revoked, .notFound:
                    // The Apple ID credential is either revoked or was not found, so show the sign-in UI.
                    print("The Apple ID credential is either revoked or was not found, so show the sign-in UI.")
                default:
                    break
                }
            }
        } else {
            // Fallback on earlier versions
        }

    }
}

extension SignInWithApple: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    public func authorizationController(controller: ASAuthorizationController,
      didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
            self.delegate?.SignInWithAppleSuccess(userIdentifier: userIdentifier)
        case let passwordCredential as ASPasswordCredential:
            let username = passwordCredential.user
            self.delegate?.SignInWithAppleSuccess(userIdentifier: username)
        default:
            self.delegate?.SignInWithAppleFail(error: nil)
            break
        }
    }

    @available(iOS 13.0, *)
    public func authorizationController(controller: ASAuthorizationController,
      didCompleteWithError error: Error) {
        self.delegate?.SignInWithAppleFail(error: error.localizedDescription)
    }
}

extension SignInWithApple: ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    public func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return UIApplication.shared.windows.filter({ $0.isKeyWindow }).last!
    }
}
