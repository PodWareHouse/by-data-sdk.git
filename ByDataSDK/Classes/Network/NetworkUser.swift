
import UIKit

public class NetworkUser: NSObject {
    private var params: [String: Any] = [:]
}

// MARK: - 21、登录注册接口
extension NetworkUser {
    
    /// 1、手机号注册
    /// - Parameters:
    ///   - number: 手机号/账号
    ///   - pwd: 密码
    ///   - checkCode: 短信验证码
    ///   - completion: 成功返回LoginModel
    ///   - fail: 失败 result = 0(网络错误)，-1(模型转换错误) ，77(手机号/账号已注册)，76(验证码错误)
    public func requestUserRegister(number: String?, pwd: String?, checkCode: String?, completion: @escaping (_ item: LoginModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("10")] = number ?? ""
        params[JsonData.paramsDicJson("11")] = pwd ?? ""
        params[JsonData.paramsDicJson("12")] = checkCode ?? ""
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_1"), parameters: params) { data, base in

            if (base.state ?? false) {
                let model: LoginModel? = data.toCodable()
                if (model != nil) {
                    NetworkTool.instance.updateData(uid: model?.user?.uid, loginKey: model?.loginKey)
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }

    }
    
    /// 2、手机号登录/微信注册登陆
    /// - Parameters:
    ///   - number: 手机号/账号
    ///   - pwd: 密码
    ///   - loginCode: 账户登录多次密码错误后，图片验证码
    ///   - completion: 成功返回LoginModel
    ///   - fail: 失败 result = 0-网络错误，-1-模型转换错误)，120-封号，74-手机号错误，716-密码错误>3次，76-图片验证码错误，715-账号/密码错误，728-登录信息过期
    public func requestUserLogin(number: String?, pwd: String?, loginCode: String?, completion: @escaping (_ item: LoginModel) -> Void, needcode: @escaping (_ error: String, _ result: Int, _ image: String) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("10")] = number ?? ""
        params[JsonData.paramsDicJson("11")] = pwd ?? ""
        if (loginCode != nil) {
            params[JsonData.paramsDicJson("13")] = loginCode!
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_2"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: LoginModel? = data.toCodable()
                if (model != nil) {
                    NetworkTool.instance.updateData(uid: model?.user?.uid, loginKey: model?.loginKey)
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                switch base.result {
                case 716, 76:
                    let model: DataCodable<String>? = data.toCodable()
                    if (model?.data != nil) {
                        needcode(base.msg ?? "", base.result ?? -1, model!.data!)
                    } else {
                        fail(base.msg ?? "", base.result ?? -1)
                    }
                default:
                    fail(base.msg ?? "", base.result ?? -1)
                }
            }
            
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2-1、第三方授权登录
    /// - Parameters:
    ///   - srcId: 第三方登录Id
    ///   - srcToken: 第三方登录token
    ///   - unionId: 第三方登录-微信unionId
    ///   - srcType: 登录类型 0-手机 1-微信 2-QQ 3-微博 4-访客 5-系统自动生成(代理使用) 6-苹果ID登录
    ///   - completion: 成功返回LoginModel
    ///   - fail: 失败 result = 0-网络错误，-1-模型转换错误，120-封号
    public func requestUserLoginAuth(srcId: String?, srcToken: String?, unionId: String?, srcType: Int, completion: @escaping (_ item: LoginModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params["src_0".params()] = srcId ?? ""
        params["src_1".params()] = srcToken ?? ""
        params["src_2".params()] = unionId ?? ""
        params["src_3".params()] = srcType
        
        NetworkTool.instance.requestPost(path: "user_2".api(), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: LoginModel? = data.toCodable()
                if (model != nil) {
                    NetworkTool.instance.updateData(uid: model?.user?.uid, loginKey: model?.loginKey)
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、退出登录
    /// - Parameters:
    ///   - completion: 成功回调
    ///   - fail: 失败回调
    public func requestUserLoginOut(completion: @escaping (_ item: BaseCodable) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] =  NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_3"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 4、登录校验接口
    /// - Parameters:
    ///   - uid: 用户uid
    ///   - loginKey: 登录校验码
    ///   - completion: 成功返回LoginModel
    ///   - fail: 失败 result = 0-网络错误，-1-模型转换错误，120-帐号被封，728-登录信息过期
    public func requestUserCheck(uid: Int?, loginKey: String?, completion: @escaping (_ item: LoginModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = uid ?? 0
        params[JsonData.paramsDicJson("17")] = loginKey ?? ""
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_4"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: LoginModel? = data.toCodable()
                if (model != nil) {
                    NetworkTool.instance.updateData(uid: model?.user?.uid, loginKey: model?.loginKey)
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 5、获取用户余额信息
    /// - Parameters:
    ///   - completion: 成功返回
    ///   - fail: 失败返回
    public func requestUserCoin(completion: @escaping (_ item: UserCoinModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_5"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: UserCoinModel? = data.toCodable()
                if (model != nil) {
                    MainData.instance.coin = model
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    
}

// MARK: - 23、用户信息相关接口
extension NetworkUser {
    
    /// 2、用户身份实名认证登记
    /// - Parameters:
    ///   - card: 身份证
    ///   - name: 姓名
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserCertificate(card: String?, name: String?,
                                       completion: @escaping (_ item: BaseCodable) -> Void,
                                       fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("52")] = card ?? ""
        params[JsonData.paramsDicJson("53")] = name ?? ""
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_22"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 4、查看自己资料页接口
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserSelf(completion: @escaping (_ item: UserInfoModel) -> Void,
                                       fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_24"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: UserInfoModel? = data.toCodable()
                if (model != nil) {
                    if (model?.user != nil) {
                        MainData.instance.model?.user = model?.user
                        MainData.instance.saveLoginModel()
                    }
                    
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }

    /// 5、更新用户信息
    /// - Parameters:
    ///   - param: 更新的数据 
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserUpdate(param: [String: Any],
                                  completion: @escaping (_ item: UserInfoModel?) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params = param
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_25"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: UserInfoModel? = data.toCodable()
                if (model?.user != nil) {
                    MainData.instance.model?.user = model?.user
                    MainData.instance.saveLoginModel()
                }
                completion(model)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 23、用户签到，注销相关接口
extension NetworkUser {
    
    /// 1、获取签到弹框内容
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserSigninBoxContent(completion: @escaping (_ item: BoxContentModel?) -> Void,
                                            fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_31"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: BoxContentModel? = data.toCodable()
                completion(model)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、签到
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserSigninSignin(completion: @escaping (_ item: AwardModel) -> Void,
                                        fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_32"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: AwardInfoCodable<AwardModel>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、获取注销提醒HTML页面内容
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserClosePage(completion: @escaping (_ link: String?) -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_35"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<String>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 4、用户申请注销账号
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserClose(completion: @escaping (_ item: BaseCodable) -> Void,
                                 fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("user_36"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 未知、更新验证码接口
extension NetworkUser {
    
    /// 1、更新验证码
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserVerificationCode(number: String,
                                            completion: @escaping (_ data: Data) -> Void,
                                            fail: @escaping (_ error: String) -> Void) {
        let path: String = JsonData.apiDicJson("user_40") + number
        params.removeAll()
        
        NetworkTool.instance.requestData(path: path) { data, error in
            if (data != nil) {
                completion(data!)
            } else {
                fail(error ?? "")
            }
        }
    }
}
