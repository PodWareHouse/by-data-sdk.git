//
//  NetworkGame.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/12/3.
//

import UIKit
import Alamofire

class NetworkGame: NSObject {
    private var params: [String: Any] = [:]
    
    public var uid: Int = 0
    public var type: Int = 0
    
    init(uid: Int?, type: Int?) {
        super.init()
        self.uid = uid ?? 0
        self.type = type ?? 0
    }
}

// MARK: - 14、街机相关接口
extension NetworkGame {
    /// 1、申请上机
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    ///   - otherHander: 跳转其它房间
    ///   - notMoneyHander: 余额不足
    ///   - fail: 失败
    public func requestGameApply(bid: Int?, oid: Int?,
                                     completion: @escaping (_ item: GApplyModel) -> Void,
                                     otherHander: @escaping (_ item: DollModel?, _ tip: String?) -> Void,
                                     notMoneyHander: @escaping () -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        params[JsonData.paramsDicJson("16")] = self.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_11"), parameters: params) { data, base in
            if (base.state ?? false) {
                switch base.result {
                case 10:
                    let model: DataCodable<DollModel>? = data.toCodable()
                    otherHander(model?.data, base.msg ?? "")
                default:
                    let model: GApplyModel? = data.toCodable()
                    if (model != nil) {
                        completion(model!)
                    } else {
                        fail("", -1)
                    }
                    break
                }
            } else {
                switch base.result {
                case 20:
                    notMoneyHander()
                default:
                    fail(base.msg ?? "", base.result ?? -1)
                    break
                }
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }

    /// 2、使用包机卡
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    ///   - otherHander: 跳转其它房间
    ///   - notMoneyHander: 余额不足
    ///   - fail: 失败
    public func requestGameFreePlay(bid: Int?, oid: Int?, pid: Int?,
                                     completion: @escaping () -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        params[JsonData.paramsDicJson("60")] = pid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_12"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、街机操作
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - opt: 操作Action-仅仅游戏机使用
    ///   - completion: 成功
    ///   - notMoneyHander: 失败
    public func requestGameOperate(bid: Int?, oid: Int?, opt: Int,
                                   completion: @escaping () -> Void,
                                   notMoneyHander: @escaping () -> Void,
                                   fail: @escaping (_ error: String) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        params[JsonData.paramsDicJson("16")] = self.uid
        params[JsonData.paramsDicJson("61")] = opt
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_13"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            }  else {
                switch base.result {
                case 20:
                    notMoneyHander()
                default:
                    break
                }
            }
        } errorHander: { error in
            fail(error)
        }
    }
    
    /// 4、投币
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - coin: 投币数量
    ///   - completion: 成功
    ///   - limitHander: 投币限制
    ///   - notMoneyHander: 余额不足
    ///   - fail: 失败
    public func requestGameCoin(bid: Int?, oid: Int?, coin: Int,
                                completion: @escaping (_ item: GCoinModel) -> Void,
                                limitHander: @escaping (_ tip: String, _ coin: Int?) -> Void,
                                notMoneyHander: @escaping () -> Void,
                                fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        params[JsonData.paramsDicJson("16")] = self.uid
        params[JsonData.paramsDicJson("62")] = coin
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_14"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: GCoinModel? = data.toCodable()
                if (model != nil) {
                    switch base.result {
                    case 10000:
                        limitHander(base.msg ?? "", model!.data)
                    default:
                        completion(model!)
                        break
                    }
                } else {
                    fail("", -1)
                }
            }  else {
                switch base.result {
                case 20:
                    notMoneyHander()
                default:
                    fail(base.msg ?? "", base.result ?? -1)
                    break
                }
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 5、机器状态
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    public func requestGameStatus(bid: Int?, oid: Int?,
                                  completion: @escaping (_ item: GStatusModel, _ state: PushState, _ head: String?, _ name: String?, _ level: String?) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_15"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<GStatusModel>? = data.toCodable()
                if (model?.data != nil) {
                    let state = self.parseGameState(item: model!.data!)
                    completion(model!.data!, state, model?.playerHeader, model?.playerName, model?.playerVIP)
                }
            }
        } errorHander: { error in }
    }
    
    /// 6、所有位置状态
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    public func requestGameAllStatus(bid: Int?, oid: Int?,
                                     completion: @escaping (_ list: [GAllStatusModel]) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        params[JsonData.paramsDicJson("16")] = self.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_16"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataListCodable<GAllStatusModel>? = data.toCodable()
                completion(model?.list ?? [])
            }
        } errorHander: { error in }
    }
    
    /// 7、下机
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestGameExit(bid: Int?, oid: Int?,
                                completion: @escaping () -> Void,
                                fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        params[JsonData.paramsDicJson("16")] = self.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_17"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 8、查询是否已结束结算
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestGameIncome(bid: Int?, oid: Int?,
                                  completion: @escaping () -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        params[JsonData.paramsDicJson("16")] = self.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("game_18"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

extension NetworkGame {
    
    /// 1、解析机器状态
    /// - Parameter item: 机器状态数据
    /// - Returns: 机器状态
    public func parseGameState(item: GStatusModel) -> PushState {
        switch item.playerUid {// 在玩UID
        case 0:
            return .free
        default:
            switch item.uid { // 在玩UID,若请求uid与在玩uid不一致则为0
            case 0:
                return .occupy
            default:
                return .play
            }
        }
    }
}
