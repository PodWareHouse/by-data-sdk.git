
import UIKit

public enum PushState {
    case none           // 未知状态
    case free           // 空闲
    case failure        // 故障
    case play           // 自己上机
    case occupy         // 别人占用
    case reserve        // 霸机
    case otherReserve   // 别人霸机
}

public class NetworkPush: NSObject {
    private var params: [String: Any] = [:]
    
    public var uid: Int = 0
    
    init(uid: Int?) {
        super.init()
        self.uid = uid ?? 0
    }
}

// MARK: - 14、推币机相关接口
extension NetworkPush {
    
    /// 1、推币机申请上机
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    ///   - otherHander: 跳转其它房间
    ///   - notMoneyHander: 余额不足
    ///   - fail: 失败
    public func requestPushApply(bid: Int?, oid: Int?,
                                     completion: @escaping (_ item: PCApplyModel) -> Void,
                                     otherHander: @escaping (_ item: DollModel?, _ tip: String?) -> Void,
                                     notMoneyHander: @escaping () -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_11"), parameters: params) { data, base in
            if (base.state ?? false) {
                switch base.result ?? -1 {
                case 10:
                    let model: DataCodable<DollModel>? = data.toCodable()
                    otherHander(model?.data, base.msg ?? "")
                default:
                    let model: PCApplyModel? = data.toCodable()
                    if (model != nil) {
                        completion(model!)
                    } else {
                        fail("", -1)
                    }
                    break
                }
            } else {
                switch base.result ?? -1 {
                case 20:
                    notMoneyHander()
                default:
                    fail(base.msg ?? "", base.result ?? -1)
                    break
                }
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、推币机投币
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    ///   - notMoneyHander: 余额不足
    ///   - applyHander: 申请上机
    ///   - fail: 失败
    public func requestPushPush(bid: Int?, oid: Int?,
                                    completion: @escaping (_ item: PCPushModel) -> Void,
                                    notMoneyHander: @escaping () -> Void,
                                    applyHander: @escaping () -> Void,
                                    fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_12"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PCPushModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                switch base.result {
                case 20:
                    notMoneyHander()
                case 24:
                    applyHander()
                default:
                    fail(base.msg ?? "", base.result ?? -1)
                    break
                }
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、操作(摆动)
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    public func requestPushOperate(bid: Int?, oid: Int?) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_13"), parameters: params) { data, base in
        } errorHander: { error in
        }
    }
    
    /// 4、推币机主动下机
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPushFinish(bid: Int?, oid: Int?,
                                      completion: @escaping () -> Void,
                                      fail: @escaping (_ error: String) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_14"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                switch base.result {
                case 24:
                    completion()
                default:
                    fail(base.msg ?? "")
                    break
                }
            }
        } errorHander: { error in
            fail(error)
        }
    }
    
    /// 5、获取用户余额信息
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功
    public func requestPushQueryAccount(bid: Int?, completion: @escaping (_ item: PCCoinModel) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_15"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<PCCoinModel>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                }
            }
        } errorHander: { error in }
    }
    
    /// 6、开宝箱
    /// - Parameters:
    ///   - bid: 机器id
    ///   - tipHander: 弹出提示信息
    ///   - fail: 失败
    public func requestPushOpenBox(bid: Int?,
                                       tipHander: @escaping (_ tip: String?) -> Void,
                                       fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_16"), parameters: params) { data, base in
            if (base.state ?? false) {
                tipHander(base.msg ?? "")
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 7、查询彩金
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPushQueryJP(bid: Int?,
                                             completion: @escaping (_ item: PCJPModel) -> Void,
                                             fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_17"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PCJPModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 8、查看宝箱值
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPushQueryBoxValue(bid: Int?,
                                             completion: @escaping (_ item: PCBoxValueModel) -> Void,
                                             fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_18"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PCBoxValueModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 9、获取机器状态
    /// - Parameters:
    ///   - busId: 机器id
    ///   - optId: 操作optId
    ///   - completion: 成功
    ///   - applyHander: 申请上机
    public func requestPushStatus(busId: Int?, optId: Int?,
                                  completion: @escaping (_ item: PCStatusModel, _ state: PushState) -> Void,
                                  applyHander: @escaping () -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = busId ?? 0
        params[JsonData.paramsDicJson("42")] = optId ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_19"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PCStatusModel? = data.toCodable()
                if (model != nil) {
                    let state: PushState = self.parsePushState(item: model!)
                    completion(model!, state)
                }
            } else {
                switch base.result {
                case 24:
                    applyHander()
                default:
                    break
                }
            }
        } errorHander: { error in }
    }
    
    /// 10、获取出奖信息
    /// - Parameters:
    ///   - bid: 机器id
    ///   - oid: 操作optId
    ///   - completion: 成功
    public func requestPushOutRecord(bid: Int?, oid: Int?,
                                         completion: @escaping (_ userCoin: Int, _ outCoin: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("42")] = oid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("push_10"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PCOutRecordModel? = data.toCodable()
                completion(model?.userCoin ?? 0, model?.outCoin ?? 0)
            }
        } errorHander: { error in }
    }
}

extension NetworkPush {
    
    /// 1、解析机器状态
    /// - Parameter item: 机器状态数据
    /// - Returns: 机器状态
    public func parsePushState(item: PCStatusModel) -> PushState {
        switch item.result {
        case 0:
            return .free
        case 2:
            return .failure
        case 3:
            if (self.uid == item.seatUid) {
                return .reserve
            } else {
                return .otherReserve
            }
        case 100:
            if (self.uid == item.data?.playerUid) {
                return .play
            } else {
                return .occupy
            }
        default:
            break
        }
        return .none
    }
}
