//
//  NetworkDoll.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/11/19.
//

import UIKit

public class NetworkDoll: NSObject {
    private var params: [String: Any] = [:]
}

// MARK: - 4、预约接口
extension NetworkDoll {
    
    /// 1、获取预约前的排位信息
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestDollPresubscribe(bid: Int,
                            tipsHander: @escaping (_ tip: String) -> Void,
                            cancelHander: @escaping () -> Void,
                            otherHander: @escaping (_ item: DollModel, _ tip: String, _ result: Int) -> Void,
                            fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("doll_11"), parameters: params) { data, base in
            if (base.state ?? false) {
                switch base.result {
                case 0:
                    // 弹出提示是否预约
                    // 预约后，您预计排在第2位，是否继续执行预约操作？
                    tipsHander(base.msg ?? "")
                case bid:
                    // 您已预约该机器，若需要取消预约，点击确定取消预约
                    cancelHander()
                default:
                    // 已预约其他机器,跳转其他到预约的机器
                    let model: DollCodable<DollModel>? = data.toCodable()
                    if (model?.data != nil) {
                        otherHander(model!.data!, base.msg ?? "", base.result ?? -1)
                    } else {
                        fail("", -1)
                    }
                    break
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、机器预约
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 预约成功
    ///   - fail: 预约失败
    public func requestDollSubscribe(bid: Int,
                                     completion: @escaping (_ tip: String) -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("doll_12"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base.msg ?? "")
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、取消预约
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 取消预约成功
    ///   - fail: 取消预约失败
    public func requestDollCancelSubscribe(bid: Int,
                                     completion: @escaping () -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("doll_13"), parameters: params) { data, base in
            completion()
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 6、机器类别、房间列表、娃娃机操作接口
extension NetworkDoll {
    
    /// 1、机器分类接口
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestDollClassify(completion: @escaping (_ list: [DollClassifyModel]) -> Void,
                                    fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("doll_21"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: ClassifyListCodable<DollClassifyModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、机器列表接口
    /// - Parameters:
    ///   - type: 分类类型
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestDollList(type: Int,
                                completion: @escaping (_ list: [DollModel]) -> Void,
                                fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("22")] = type
        params[JsonData.paramsDicJson("30")] = 1
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("doll_22"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: ListCodable<DollModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    // 8、
    public func requestDollQuery(bid: Int?,
                                 completion: @escaping (_ item: DollModel) -> Void,
                                 fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("doll_28"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: InfoCodable<DollModel>? = data.toCodable()
                if (model != nil && model!.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 9、观众列表接口
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestDollWatchers(bid: Int?,
                                    completion: @escaping (_ list: [UserModel]) -> Void,
                                    fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("30")] = 1
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("doll_29"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: WatchersListCodable<UserModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}
