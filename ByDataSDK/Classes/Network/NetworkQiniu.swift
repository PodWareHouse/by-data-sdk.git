//
//  NetworkQiniu.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/11/18.
//

import UIKit
import Alamofire
import Qiniu

public class NetworkQiniu: NSObject {
    
    private var params: [String: Any] = [:]
}

// MARK: - 1、上传相关
extension NetworkQiniu {
    
    /// 1、获取七牛上传token接口
    /// - Parameters:
    ///   - type: 七牛token上传类型 1-聊天IM 2-用户头像 3-动态话题
    ///   - msgType: 七牛token上传类型 1-聊天IM （上传图片时，需传入msgType=2）
    ///   - ext: 扩展字段信息: 使用type=1的时候，要在请求时ext=(发送者)uid-tid(接收者) 20181114-10000;上传时，需将x开头的字段置入魔法变量中去在客户端上传的时候，增加魔法变量x:msgType (1=图片 2=视频)
    ///   - completion: 成功回调
    ///   - fail: 失败回调
    private func requestQiniuToken(type: Int, msgType: Int, ext: String?, completion: @escaping (_ item: QiniuTokenModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("22")] = type
        params[JsonData.paramsDicJson("21")] = msgType
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        if (type == 1) {
            params[JsonData.paramsDicJson("20")] = ext ?? ""
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("qi_1"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<QiniuTokenModel>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    private func requestUpload(data: Data, model: QiniuTokenModel, completion: @escaping (_ item: QiniuRespModel?) -> Void, fail: @escaping (_ error: String) -> Void) {

        var item: QiniuParamsModel = QiniuParamsModel()
        item.x_type = String(model.x_type ?? 0)
        item.x_uid = String(model.x_uid ?? 0)
        item.x_msgType = "1"
        item.x_ext = model.x_ext
        item.x_fileName = model.fileName
        item.x_token = model.token
        item.x_loginKey = MainData.instance.model?.loginKey
        
        let key: String = (model.key ?? "") + (model.fileName ?? "")
        let dic: [AnyHashable: Any]? = item.toDictionary()
        
        if (dic != nil) {
            let option = QNUploadOption(mime: "image/plain", progressHandler: nil, params: dic!, checkCrc: true, cancellationSignal: nil)
            let manager: QNUploadManager = QNUploadManager()
            manager.put(data, key: key, token: model.token ?? "", complete: { info, key, diction in
                if (diction != nil) {
                    let resp: DataCodable<QiniuRespModel>? = diction!.toData()?.toCodable()
                    completion(resp?.data)
                } else {
                    fail(info?.error.localizedDescription ?? "")
                }
            }, option: option)
        } else {
            fail("")
        }
    }
}

// MARK: - 1、上传图片
extension NetworkQiniu {
    
    /// 1、上传头像
    /// - Parameters:
    ///   - data: 头像
    ///   - completion: 完成
    ///   - fail: 失败
    public func requestUploadAvator(data: Data, completion: @escaping (_ item: QiniuRespModel?) -> Void, fail: @escaping (_ error: String) -> Void) {
        self.requestQiniuToken(type: 2, msgType: 2, ext: nil) { item in

            self.requestUpload(data: data, model: item, completion: completion, fail: fail)
            
        } fail: { error, result in
            fail(error)
        }
    }
    
    /// 1、上传聊天图片
    /// - Parameters:
    ///   - data: 头像
    ///   - completion: 完成
    ///   - fail: 失败
    public func requestUploadChatImage(data: Data, completion: @escaping (_ item: QiniuRespModel?) -> Void, fail: @escaping (_ error: String) -> Void) {
        let ext: String = String(MainData.instance.model?.user?.uid ?? 0) + "-" + String(MainData.instance.im.customer?.user?.gameUid ?? 0)
        self.requestQiniuToken(type: 1, msgType: 2, ext: ext) { item in
            
            self.requestUpload(data: data, model: item, completion: completion, fail: fail)
            
        } fail: { error, result in
            fail(error)
        }
    }
}
