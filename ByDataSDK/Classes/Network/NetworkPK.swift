//
//  NetworkPK.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/12/14.
//

import UIKit

public class NetworkPK: NSObject {
    private var params: [String: Any] = [:]
}

// MARK: - 30、PK相关接口信息
extension NetworkPK {
    
    public var rulelink: String {
        return "http://" + (NetworkTool.instance.http?.host ?? "") + "/" + "pk_10".api()
    }
    
    /// 1、获取PK页面信息
    /// - Parameters:
    ///   - page: 页码，起始页码=1
    ///   - searchStr: 搜索的房间ID, 现改成用户ID
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkRoomList(page: Int, searchStr: String?,
                                  completion: @escaping (_ item: PKListModel) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("30")] = page
        if (searchStr != nil) {
            params[JsonData.paramsDicJson("90")] = searchStr
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_1"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PKListModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、获取PK某个房间详情信息
    /// - Parameters:
    ///   - roomid: PK房间ID
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkRoomDetail(roomid: Int,
                                    completion: @escaping (_ item: PKInfoModel) -> Void,
                                    fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("91")] = roomid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_2"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PKInfoModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、获取发起PK页信息
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkCreateRequire(completion: @escaping (_ item: PKCreateInfoModel) -> Void,
                                       fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_3"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PKCreateInfoModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
  
    /// 4、获取机器列表，根据机器类型-房间倍数
    /// - Parameters:
    ///   - busType: 机器类型：1-推币机 2-游戏机
    ///   - roomid: 参与的时候，传入pkRoomId
    ///   - price: 机器倍数
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkBusList(busType: Int?, roomid: Int?, price: Int?,
                                 completion: @escaping (_ list: [DollModel]) -> Void,
                                 fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        if (roomid == nil) {
            params[JsonData.paramsDicJson("63")] = busType ?? 0
            params[JsonData.paramsDicJson("92")] = price ?? 0
        } else {
            params[JsonData.paramsDicJson("91")] = roomid ?? 0
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_4"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: ListCodable<DollModel>? = data.toCodable()
                if (model != nil) {
                    completion(model!.list ?? [])
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 5、发起PK
    /// - Parameters:
    ///   - busid: 申请开始pk的机器ID
    ///   - cardType: pk卡类型：卡类型：1-初级(适用1-10倍房) 2-中级(适用11-50倍房) 3-高级(适用>50倍房)
    ///   - envType: 发起PK形式：1-PK活动页发起 2-房间内发起
    ///   - maxJoin: 设置的最高参与人数0表示不限制
    ///   - pwd: 约战密码(房间密码)
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkCreate(busid: Int?, cardType: Int, envType: Int, maxJoin: Int, pwd: String?,
                                completion: @escaping (_ item: DollModel?, _ msg: String) -> Void,
                                fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = busid ?? 0
        params[JsonData.paramsDicJson("93")] = cardType
        params[JsonData.paramsDicJson("94")] = envType
        params[JsonData.paramsDicJson("95")] = maxJoin
        if (pwd != nil) {
            params[JsonData.paramsDicJson("96")] = pwd!
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_5"), parameters: params) { data, base in
            let model: DollBusCodable<DollModel>? = data.toCodable()
            if (model?.state ?? false) {
                print("发起PK - 1")
                completion(model?.data, base.msg ?? "")
            } else {
                print("发起PK - 2")
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            print("发起PK - 3")
            fail(error, 0)
        }
    }
    
    /// 6、参与PK
    /// - Parameters:
    ///   - busid: 上机的房间
    ///   - pwd: 约战密码(房间密码)
    ///   - roomid: pk房间ID
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkJoin(busid: Int?, pwd: String?, roomid: Int?,
                              completion: @escaping (_ item: DollModel) -> Void,
                              fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = busid ?? 0
        params[JsonData.paramsDicJson("91")] = roomid ?? 0
        if (pwd != nil) {
            params[JsonData.paramsDicJson("96")] = pwd!
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_6"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PKJoinModel? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else if (model?.dollBus != nil) {
                    completion(model!.dollBus!)
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 7、获取PK的分享信息
    /// - Parameters:
    ///   - roomid: pk房间ID
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkShareInfo(roomid: Int?,
                                   completion: @escaping (_ item: PKShareModel) -> Void,
                                   fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("91")] = roomid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_7"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PKShareModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 8、房间是否有PK卡
    /// - Parameters:
    ///   - busid: PK房间ID, 若在PK活动页则直接传入0
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkBusCard(busid: Int?,
                                 completion: @escaping (_ item: PKRoomModel) -> Void,
                                 fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = busid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_8"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: PKRoomModel = data.toCodable() ?? PKRoomModel()
                completion(model)
            } else {
                completion(PKRoomModel())
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 9、校验密码是否正确
    /// - Parameters:
    ///   - pwd: 约战密码(房间密码)
    ///   - roomid: pk房间ID
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPkCheckPwd(pwd: String?, roomid: Int?,
                              completion: @escaping (_ item: BaseCodable) -> Void,
                              fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("91")] = roomid ?? 0
        if (pwd != nil) {
            params[JsonData.paramsDicJson("96")] = pwd!
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("pk_9"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}
