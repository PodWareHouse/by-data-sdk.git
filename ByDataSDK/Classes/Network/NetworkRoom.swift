//
//  NetworkRoom.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/11/19.
//

import UIKit

public class NetworkRoom: NSObject {
    private var params: [String: Any] = [:]
}

// MARK: - 3、房间聊天接口
extension NetworkRoom {
    /// 1、房间聊天
    /// - Parameters:
    ///   - bid: 机器id
    ///   - content: 聊天内容
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestRoomDoChat(bid: Int?, content: String, completion: @escaping (_ item: BaseCodable) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("32")] = content
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_11"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、获取房间聊天记录
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestRoomGetChat(bid: Int?, completion: @escaping (_ list: [RoomChatModel]) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_12"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataListCodable<RoomChatModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 5、霸机接口
extension NetworkRoom {
    
    /// 1、获取霸机权限
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功 【1 任性霸机 2故障霸机】
    ///   - fail: 失败
    public func requestSeatPermission(bid: Int?, completion: @escaping (_ list: [Int]) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_21"), parameters: params) { data, base in
            if (base.state ?? false) {
                switch base.result {
                case 1:
                    completion([1])
                case 2:
                    completion([1,2])
                default:
                    fail("", -1)
                    break
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、霸机前提示信息
    /// - Parameters:
    ///   - bid: 机器id
    ///   - seatType: 1 任性霸机 2故障霸机
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestSeatPreset(bid: Int?, seatType: Int, completion: @escaping (_ tip: String) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("41")] = seatType
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_22"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base.msg ?? "")
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、进行霸机
    /// - Parameters:
    ///   - bid: 机器id
    ///   - seatType: 1 任性霸机 2故障霸机
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestSeatSeat(bid: Int?, seatType: Int, completion: @escaping (_ item: SeatSetModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("41")] = seatType
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_23"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<SeatSetModel>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 15、故障报修
extension NetworkRoom {
    /// 1、报修申请
    /// - Parameters:
    ///   - bid: 机器id
    ///   - tipHander: 弹出提示信息
    public func requestRepairReport(bid: Int?, tipHander: @escaping (_ tip: String?) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        params[JsonData.paramsDicJson("63")] = 1
        params[JsonData.paramsDicJson("64")] = " "
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_31"), parameters: params) { data, base in
            tipHander(base.msg ?? "")
        } errorHander: { error in
            tipHander(error)
        }
    }
    
}

// MARK: - 24、免费币玩法接口
extension NetworkRoom {
    
    /// 1、获取免费玩法权益
    /// - Parameters:
    ///   - bid: 机器id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestFreePlayPermission(bid: Int?,
                                         completion: @escaping (_ list: [FreePlayModel]) -> Void,
                                         fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_41"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataListCodable<FreePlayModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、开始免费时长玩法
    /// - Parameters:
    ///   - bid: 机器id
    ///   - perid: 用户免费时长权益ID
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestFreePlay(bid: Int?, perid: Int?,
                                         completion: @escaping () -> Void,
                                         fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("40")] = bid ?? 0
        params[JsonData.paramsDicJson("60")] = perid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("room_42"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}


