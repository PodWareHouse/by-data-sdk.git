
import UIKit
import Alamofire

public class NetworkTool: NSObject {
    static public let instance = NetworkTool()
    
    public var headkey: String?
    public var head: HTTPHeadKey?
    public var http: HTTPModel?
    public var uid: Int = 0
    
    
    private var url: String?
    
    private override init() {
        super.init()
    }
    
    public func initData(http: HTTPModel, head: HTTPHeadKey) {
        self.head = head
        self.http = http
        
        self.url = "http://" + http.host + "/" + http.port + "/"
        self.headkey = head.toJSONString()
        self.uid = head.uid
        
        self.requestAppCnf()
    }

    public func updateData(uid: Int?, loginKey: String?) {
        self.uid = uid ?? 0
        self.head?.uid = uid ?? 0
        self.head?.loginKey = loginKey ?? ""
        self.headkey = head?.toJSONString()
    }
}

private extension NetworkTool {
    
    /// 1、默认请求系统配置信息
    func requestAppCnf() {
        NetworkApp().requestAppCnf { item in
            
        } fail: { error, result in
            
        }
    }
}

// MARK: - 一、基础请求
extension NetworkTool {

    /// 1、POST请求
    /// - Parameters:
    ///   - path: 请求地址
    ///   - parameters: 请求参数
    ///   - requestHander: 请求回调
    ///   - errorHander: 网络错误回调
    func requestPost(path: String, parameters: Parameters?, requestHander: @escaping (_ data: Data, _ base: BaseCodable) -> (), errorHander: @escaping (_ error: String) -> ()) {
        let header: HTTPHeaders = ["headKey": headkey ?? ""]
        let link: String = url! + path
        
        AF.request(link,
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: header,
                   requestModifier: { $0.timeoutInterval = 5 })
            .responseData { response in
                switch response.result {
                case let .success(data):
                    let base: BaseCodable? =  data.toCodable()
                    if (base != nil) {
                        // 120-帐号被封，728-登录信息过期
                        switch base?.result {
                        case 120, 728:
                            errorHander("timeout")
                            MainData.instance.requestToLogout {
                                MainData.instance.delegate?.MainDataDidReceiveTimeOut(base?.msg)
                            }
                        default:
                            requestHander(data, base!)
                        }
                    } else {
                        errorHander("To Codable Error ～")
                    }
                case let .failure(error):
                    errorHander(error.localizedDescription)
                }
            }
    }
    
    /// 2、Get获取data
    /// - Parameters:
    ///   - path: 请求地址
    ///   - requestHander: 请求回调
    func requestData(path: String, requestHander : @escaping ( _ data : Data?, _ error: String?) -> ()) {
        let link: String = url! + path
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 5.0
        let sess = URLSession(configuration: config)
        let task = sess.dataTask(with: URL(string: link)!, completionHandler: {(data,response,error)->Void in
            requestHander(data, error?.localizedDescription)
            })
        task.resume()
    }
    
    
}

// MARK: - 2、外部调用
public extension NetworkTool {
    static func request(method: HTTPMethod, url: String, parameters: Parameters?, requestHander: @escaping (_ data: Data) -> (), errorHander: @escaping (_ error: String) -> ()) {
        AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: { $0.timeoutInterval = 10 })
            .responseData { response in
                switch response.result {
                case let .success(data):
                    requestHander(data)
                case let .failure(error):
                    errorHander(error.localizedDescription)
                }
            }
    }
}
