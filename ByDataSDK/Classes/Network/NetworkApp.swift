//
//  NetworkApp.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/11/19.
//

import UIKit

public class NetworkApp: NSObject {
    private var params: [String: Any] = [:]
}

// MARK: - 1、获取系统配置+聊天置顶
extension NetworkApp {
    
    /// 5、app系统配置接口NEW
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestAppCnf(completion: @escaping (_ item: AppInfoModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_05"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<AppInfoModel>? = data.toCodable()
                if (model?.data != nil) {
                    MainData.instance.appinfo = model?.data
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 2、获取banner信息
extension NetworkApp {
    /// 1、获取banner接口
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestBannerList(completion: @escaping (_ banners: [BannerModel], _ records: [RecordModel]) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_1"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: BannerListCodable<BannerModel, RecordModel>? = data.toCodable()
                completion(model?.banners ?? [], model?.records ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 2-1、获取活动列表
extension NetworkApp {
    /// 1、活动列表接口
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestActivitiesList(completion: @escaping (_ list: [ActivityModel]) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_21"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: ActivityListCodable<ActivityModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、执行活动列表请求
    /// - Parameters:
    ///   - target: 跳转目标
    ///   - targetId: 跳转目标Code
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestActivitiesAction(target: Int?, targetId: Int?, completion: @escaping (_ item: BaseCodable) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params["82".params()] = target ?? 0
        params["83".params()] = target ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_22"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 12、邀请接口
extension NetworkApp {
    
    /// 1、我的邀请码页面接口
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestInviteMine(completion: @escaping (_ item: InviteMineModel) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_31"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: InviteMineModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、领取邀请奖励接口
    /// - Parameters:
    ///   - rewardId: 奖励id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestInviteReward(rewardId: Int?, completion: @escaping (_ item: BaseCodable) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        params[JsonData.paramsDicJson("50")] = rewardId ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_32"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、进入填写邀请码页面接口
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestInviteCode(completion: @escaping (_ item: InviteCodeModel) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_33"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: InviteCodeModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 4、填写邀请码接口
    /// - Parameters:
    ///   - inviteCode: 邀请码
    ///   - type: 邀请码类型 1-用户邀请码 2-官方邀请码
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestInviteAdd(inviteCode: String,
                                 type: Int,
                                 completion: @escaping (_ msg: String) -> Void,
                                 fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        params[JsonData.paramsDicJson("51")] = inviteCode
        params[JsonData.paramsDicJson("22")] = type
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_34"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base.msg ?? "")
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 18、社交部分-话题圈子动态接口
extension NetworkApp {
    /// 19、用户反馈
    /// - Parameters:
    ///   - content: 反馈内容
    ///   - number: 反馈手机号
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestSocialFeedback(content: String, number: String?,
                                      completion: @escaping (_ tip: String) -> Void,
                                      fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("32")] = content
        if (number != nil) {
            params[JsonData.paramsDicJson("10")] = number
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_419"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base.msg ?? "")
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 21、更新个人信息
    /// - Parameters:
    ///   - dic: 内容
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestSocialUserEdit(dic: [String: Any],
                                      completion: @escaping () -> Void,
                                      fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params = dic
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_421"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 22、关于我们
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestSocialAbout(completion: @escaping (_ item: AboutModel) -> Void,
                                   fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_422"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: AboutCodable<AboutModel>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    
}

// MARK: - 19、充值
extension NetworkApp {
    
    /// 1、根据分钟数，获取包机卡房间倍数的价格信息
    /// - Parameters:
    ///   - code: 获取包机卡列表传入的code
    ///   - uid: 用户id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestChargeCharter(code: Int?,
                                     completion: @escaping (_ list: [ChargeCharaterModel]) -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        params[JsonData.paramsDicJson("70")] = code ?? 0
        params[JsonData.paramsDicJson("71")] = 1
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_51"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataListCodable<ChargeCharaterModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、根据code获取支付详情【非包机卡】
    /// - Parameters:
    ///   - code: 商品代码
    ///   - uid: 用户id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestChargeCode(code: Int?,
                                  completion: @escaping (_ item: ChargeItemModel) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        params[JsonData.paramsDicJson("70")] = code ?? 0
        params[JsonData.paramsDicJson("71")] = 1
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_52"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: CatalogsCodable<ChargeItemModel>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、获取充值列表接口-小鱼使用
    /// - Parameters:
    ///   - uid: 用户id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestChargeIos(completion: @escaping (_ item: ChargeListModel) -> Void,
                                 fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_53"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: ChargeListModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 4、获取充值列表接口
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestChargeList(completion: @escaping (_ item: ChargeListModel) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_54"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: ChargeListModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 5、内购充值-获取内购产品ID
    /// - Parameters:
    ///   - code: 商品代码
    ///   - uid: 用户id
    ///   - freePlay: 是否购买包机权益 0 否 1是
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestChargeOrder(code: Int?, freePlay: Int?,
                                     completion: @escaping (_ produceID: String) -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("70")] = code ?? 0
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        params[JsonData.paramsDicJson("72")] = freePlay ?? 0
        params[JsonData.paramsDicJson("73")] = 3
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_55"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<String>? = data.toCodable()
                completion(model?.data ?? "")
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 20、账单
extension NetworkApp {

    /// 1、账单接口
    /// - Parameters:
    ///   - page: 页
    ///   - uid: 用户id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestTradeList(page: Int,
                                 completion: @escaping (_ list: [TradeListModel], _ coin: Int) -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("16")] = NetworkTool.instance.uid
        params[JsonData.paramsDicJson("30")] = page
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_61"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: TradeListData? = data.toCodable()
                completion(model?.list ?? [], model?.coin ?? 0)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 22、验证码接口
extension NetworkApp {
    
    /// 1、
    /// - Parameters:
    ///   - type: 验证码类型 1-注册 2-绑定手机 3-更换绑定 4-更改密码
    ///   - number: 手机号
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPhoneCheckCode(type: Int, number: String,
                                     completion: @escaping (_ item: BaseCodable) -> Void,
                                     fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("54")] = type
        params[JsonData.paramsDicJson("10")] = number
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_71"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、手机重置密码
    /// - Parameters:
    ///   - code: 验证码
    ///   - number: 手机号码
    ///   - pwd: 密码
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPhoneResetPwd(code: String, number: String, pwd: String,
                                    completion: @escaping (_ item: BaseCodable) -> Void,
                                    fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("12")] = code
        params[JsonData.paramsDicJson("10")] = number
        params[JsonData.paramsDicJson("11")] = pwd
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_72"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、手机用户绑定
    /// - Parameters:
    ///   - code: 验证码
    ///   - number: 手机号码
    ///   - uid: 用户id
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPhoneUserRecord(code: String, number: String, uid: Int?,
                                      completion: @escaping (_ item: BaseCodable) -> Void,
                                      fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("12")] = code
        params[JsonData.paramsDicJson("10")] = number
        params[JsonData.paramsDicJson("16")] = uid ?? 0
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_73"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 25、内购数据校验接口
extension NetworkApp {
    
    /// 1、内购数据校验接口
    /// - Parameters:
    ///   - text: 校验数据
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPayVerify(text: String,
                                completion: @escaping () -> Void,
                                fail: @escaping (_ error: String) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("55")] = text
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_81"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "")
            }
        } errorHander: { error in
            fail(error)
        }
    }
}

// MARK: - 27、榜单接口
extension NetworkApp {
    
    /// 1、app新榜单
    /// - Parameters:
    ///   - category: 榜单类别 1-日榜 2-周榜 3-月榜
    ///   - type: 榜单类型 1-(充值)豪气榜 2-财富榜 3-小丑榜 4-南瓜榜
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestRankingList(category: Int, type: Int,
                                  completion: @escaping (_ item: RankCodableModel) -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("80")] = category
        params[JsonData.paramsDicJson("81")] = type
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_91"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: RankCodableModel? = data.toCodable()
                if (model != nil) {
                    completion(model!)
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 1、app领取榜单奖励
    /// - Parameters:
    ///   - category: 榜单类别 1-日榜 2-周榜 3-月榜
    ///   - type: 榜单类型 1-(充值)豪气榜 2-财富榜 3-小丑榜 4-南瓜榜
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestRankingReward(category: Int, type: Int,
                                  completion: @escaping () -> Void,
                                  fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("80")] = category
        params[JsonData.paramsDicJson("81")] = type
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_92"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 28、用户卡包相关接口
extension NetworkApp {
    
    /// 1、激活周卡月卡
    /// - Parameters:
    ///   - cardid: 激活的周卡月卡卡ID
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUseCard(cardid: Int,
                                    completion: @escaping () -> Void,
                                    fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("56")] = cardid
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_101"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion()
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、获取用户卡包列表
    /// - Parameters:
    ///   - page: 页码，当前每页10条数据
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestUserCard(page: Int,
                               completion: @escaping (_ list: [CardModel]) -> Void,
                               fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("30")] = page
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_102"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: CardListCodable<CardModel>? = data.toCodable()
                if (model != nil) {
                    completion(model?.list ?? [])
                } else {
                    fail(base.msg ?? "", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}


// MARK: - 未知、战况
extension NetworkApp {

    /// 1、获取战况列表
    /// - Parameters:
    ///   - type: 类别 0-金币 1-JP
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestPrizeList(type: Int, page: Int?, id: Int?,
                               completion: @escaping (_ list: [PrizeModel]) -> Void,
                               fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("22")] = type
        if (page != nil) {
            params[JsonData.paramsDicJson("30")] = page!
        }
        if (id != nil) {
            params[JsonData.paramsDicJson("33")] = id!
        }
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("app_120"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataListCodable<PrizeModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}
