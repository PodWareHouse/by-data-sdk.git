//
//  NetworkCms.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/11/18.
//

import UIKit

public class NetworkCms: NSObject {
    private var params: [String: Any] = [:]
}

// MARK: - 1、客服聊天
extension NetworkCms {
    /// 1、拉取我的信息
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestCmsUserApp(completion: @escaping (_ item: CmsModel) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path: JsonData.apiDicJson("cms_11"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataCodable<CmsModel>? = data.toCodable()
                if (model?.data != nil) {
                    completion(model!.data!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 2、拉取好友（用户拉取客服/客服拉取用户）列表
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestCmsFriendList(completion: @escaping (_ list: [CmsFriendsModel]) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("30")] = 1
        
        NetworkTool.instance.requestPost(path:  JsonData.apiDicJson("cms_12"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataListCodable<CmsFriendsModel>? = data.toCodable()
                completion(model?.list ?? [])
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 3、发起对话
    /// - Parameters:
    ///   - tid: 目标的用户uid（gameUid）
    ///   - content: 对话内容
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestCmsSendText(tid: Int, content: String, completion: @escaping (_ item: BaseCodable) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("32")] = content
        params[JsonData.paramsDicJson("31")] = tid
        params[JsonData.paramsDicJson("21")] = 0
        
        NetworkTool.instance.requestPost(path:  JsonData.apiDicJson("cms_13"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 4、聊天记录
    /// - Parameters:
    ///   - tid: 目标的用户uid（gameUid）
    ///   - page: 分页
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestCmsHistoryList(tid: Int, page: Int, completion: @escaping (_ list: [CmsChatModel]) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("30")] = page
        params[JsonData.paramsDicJson("31")] = tid
        
        NetworkTool.instance.requestPost(path:  JsonData.apiDicJson("cms_14"), parameters: params) { data, base in
            if (base.state ?? false) {
                let model: DataListCodable<CmsChatModel>? = data.toCodable()
                if (model?.list != nil) {
                    completion(model!.list!)
                } else {
                    fail("", -1)
                }
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
    
    /// 5、更新已读消息
    /// - Parameters:
    ///   - tid: 目标的用户uid（gameUid）
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestCmsIsRead(tid: Int, completion: @escaping (_ item: BaseCodable) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        params[JsonData.paramsDicJson("31")] = tid
        
        NetworkTool.instance.requestPost(path:  JsonData.apiDicJson("cms_15"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}

// MARK: - 13、更新友盟Token
extension NetworkCms {
    /// 1、请求发送欢迎消息推送
    /// - Parameters:
    ///   - completion: 成功
    ///   - fail: 失败
    public func requestMsgWelcome(completion: @escaping (_ item: BaseCodable) -> Void, fail: @escaping (_ error: String, _ result: Int) -> Void) {
        params.removeAll()
        
        NetworkTool.instance.requestPost(path:  JsonData.apiDicJson("cms_21"), parameters: params) { data, base in
            if (base.state ?? false) {
                completion(base)
            } else {
                fail(base.msg ?? "", base.result ?? -1)
            }
        } errorHander: { error in
            fail(error, 0)
        }
    }
}
