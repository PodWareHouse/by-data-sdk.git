
import Foundation

struct QiniuTokenModel: Codable {
    var x_type: Int?
    var x_uid: Int?
    var x_ext: String?
    
    var key: String?
    var token: String?
    var fileName: String?
    
    enum CodingKeys: String, CodingKey {
        case x_type = "x:type"
        case x_uid = "x:uid"
        case x_ext = "x:ext"
        case key
        case token
        case fileName
    }
}

struct QiniuParamsModel: Codable {
    var x_type: String?
    var x_uid: String?
    var x_msgType: String?
    var x_ext: String?
    
    var x_fileName: String?
    var x_token: String?
    var x_loginKey: String?
    
    enum CodingKeys: String, CodingKey {
        case x_type = "x:type"
        case x_uid = "x:uid"
        case x_msgType = "x:msgType"
        case x_ext = "x:ext"
        case x_fileName = "x:fileName"
        case x_token = "x:token"
        case x_loginKey = "x:loginKey"
    }
}

public struct QiniuRespModel: Codable {
    public var url: String?
}
