//
//  AppCodable.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/11/19.
//

import UIKit

public struct AppInfoModel: Codable {
    public var privacyLink: String?
    public var protocolLink: String?
    
    enum CodingKeys: String, CodingKey {
        case privacyLink = "privacy"
        case protocolLink = "protocol"
    }
}

public struct BannerModel: Codable {
    var title: String?
    var targetTitle: String?
    public var url: String?
    public var imgUrl: String?
    public var target: Int?
    public var targetId: Int?
}

public struct RecordModel: Codable {

    public var name: String?
    public var head: String?
    public var busId: Int?
    public var dollName: String?
    public var type: Int?
    var time: Int?
}

public struct ActivityModel: Codable {
    public var title: String?      // 活动标题
    public var url: String?        // 页面H5链接
    public var imgUrl: String?     // 图片链接
    public var target: Int?        // 跳转目标
    public var targetId: Int?      // 跳转目标id
    public var describe: String?   // 说明文案
    var startTime: String?  // 开始时间
    public var endTime: String?    // 结束时间
    public var action: Int?        // 按钮动作类型：1-执行跳转动作 2-执行请求API
    public var status: Int?        // 按钮显示状态：0-立即参与 1-已领取 2-已结束
}

public struct RoomChatModel: Codable {
    public var name: String?
    public var content: String?
    
    public init(_ name: String?, _ content: String?) {
        self.name = name
        self.content = content
    }
}

public struct InviteMineModel: Codable {
    public var invitedCode: Int?           // 我的邀请码,邀请码>0即可以邀请注册，否则是引导微信注册
    public var invitedHtml: String?        // 邀请码右上角帮助内容描述
    public var qrcode: String?             // 邀请二维码
    public var inviteRewardDes: String?    // 邀请好友各得20币的描述
    public var shareRewardDes: String?     // 分享合成图底部奖励的描述
    var invitesuccessDes: String?   // 助攻列表空时描述
    public var srcollList: [String]?       // 滚动栏
    public var inviteList: [InviteRewardModel]?    // 邀请奖励列表
    public var successList: [InviteRewardModel]?   // 助攻奖励列表
}

public struct InviteRewardModel: Codable {
    public var id: Int?
    public var reward: Int?        // 可获得奖励游戏币
    public var state: Int?         // 奖励状态：0-未满足条件 1-待领取 2-已领取
    public var time: TimeInterval?
    public var remark: String?
    public var name: String?
    public var head: String?
}

public struct InviteCodeModel: Codable {
    var showInput: Bool?
    var invitedCode: Int?
    public var invitedHtml: String?
    var srcollList: [String]?
}

public struct AboutModel: Codable {
    public var about: String?      // 简介
    public var version: String? 
    public var qrCode: String?     // 二维码
    public var qrTarget: String?   // 二维码类型：如QQ： / 客服微信：
    public var qrText: String?     // 二维码文本
}

public struct ChargeCharaterModel: Codable {
    public var id: Int?
    public var name: String?
    public var price: Int?
    public var showTitle: String?
    public var showName: String?
    public var showSubTitle: String?
    public var applePayPID: String?
}

public struct ChargeItemModel: Codable {
    public var applePayPID: String?    // 内购支付id
    public var cardTitle: String?      // 劲爆周卡，双倍月卡, 包机卡
    public var code: Int?              // 充值代码
    public var des: String?            // 描述
    public var itemDetail: String?     // 商品详情描述
    public var name: String?           // 项目名称
    public var price: Int?             // 售价
    public var coin: Int?              // 
    var showFirst: Int?         // 是否对首充做限制 0-不起作用 1-显示限时，但可一直充值
    public var isFirst: Int?           // 首冲优惠 0-常规充值 1-首冲优惠 2-周卡 3-月卡 4-包机时长 5-新周卡 6-新月卡 7-超级月卡 8-PK卡
    public var tag: Int?               // 充值标签 0-常规充值 1-快捷充值 2-包机时长 3-PK卡
    public var pkCardType: Int?        // pk卡类型 1-初级 2-中级 3-高级
}

public struct ChargeListModel: Codable {
    public var specialCatalogs: [ChargeItemModel]?  // 特殊优惠清单
    public var pkCatalogs: [ChargeItemModel]?       // PK卡列表
    public var acrPlayCatalogs: [ChargeItemModel]?  // 街机包机卡
    public var freePlayCatalogs: [ChargeItemModel]? // 包机时长权益包
    public var catalogs: [ChargeItemModel]?         // 商品清单
    public var coin: Int?
}

public struct TradeListData: Codable, Hashable {
    public var list: [TradeListModel]?
    public var coin: Int?
}

public struct TradeListModel: Codable, Hashable {
    public var recordType: Int?    // 交易货币类型 0消费 1获得
    public var cost: Int?          // 交易金额
    public var remark: String?     // 交易描述
    public var time: Int?          //
}

public struct FreePlayModel: Codable {
    public var id: Int?
    public var name: String?
}

public struct RankCodableModel: Codable {
    public var rankItemList: [RankModel]?   // 榜单列表
    public var selfRank: RankModel?         // 自己的榜单数据
}

public struct RankModel: Codable {
    public var amount: CGFloat?    // 数值
    public var hasReward: Bool?// 是否可以领取奖励
    public var head: String?   // 用户头像,豪气榜会返回空
    public var name: String?   // 用户名称,豪气榜会返回****
    public var rank: Int?      // 排名
    public var rewardDescribe: String? // 奖励描述
    public var rewardNum: Int? // 奖励数值，若上榜，则必有值
    var uid: Int?       // 用户ID,豪气榜会返回0
    public var vip: String?    // vip等级
}

public struct CardModel: Codable {
    public var addDate: String?    // 获取时间
    var cardId: Int?        // 卡ID 因包机卡与月卡存在不同的表，可能存在重复ID
    public var cardLimit: String?  // 限制说明，仅仅包机卡使用 example: 限10倍房使用
    public var cardType: Int?      // 卡类型 1-周卡 2-月卡 3-新周卡 4-新月卡 5-超级月卡 10-包机卡 20-初级PK卡 21-中级PK卡 22-高级PK卡
    public var rule: String?       // 描述 example: 包机卡使用后，可得50W免费币；倒计时结束后，免费币大于50W部分为赢得游戏币；免费币余额小于50W则不得到游戏币
    public var status: Int?        // 使用状态 0未使用 1使用中 2已使用
    public var title: String?      // 标题 example: 10分钟游戏包机卡；新月卡
    public var useDate: String?    // 使用时间-PK卡使用
    public var validDate: String?  // 有效期，仅使用中的周卡/月卡使用
}

public struct PrizeModel: Codable {
    public var id: Int?
    public var name: String?
    public var head: String?
    public var busId: Int?
    public var time: Int?
    public var title: String?  // 单位 如 金币；JP1 JP2 JP3 全盘奖
    public var num: Int?   // 数量 如 30,不存在时，不用拼接
    public var prizeType: Int? // 类型：1-推币 2-游戏机 3-JP 4-pk
    
    public var rank: Int?
    public var pkId: Int?
    public var pkUName: String?
    
}

