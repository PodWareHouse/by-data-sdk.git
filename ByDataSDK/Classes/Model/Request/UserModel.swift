
import Foundation

public struct LoginModel: Codable {
    public var loginKey: String?
    public var user: UserModel?
    var connector: ConnectorModel?
    public var verify: Int? // 是否需要弹出实名认证窗口【0-不弹出 1-弹出可以取消 2-弹出取消后强制退出】【已实名登记的后台会返回0】
    public var certificate: Int? // 1-身份未实名认证 2-身份已实名认证
}

public struct UserModel: Codable {
    public var uid: Int?
    public var name: String?
    public var head: String?
    var about: String?
    var number: Int?
    public var gender: Int?
}

public struct ConnectorModel: Codable {
    var userName: String?
    var password: String?
    var clientId: String?
    var server: ServerModel?
}

public struct ServerModel: Codable {
    var host: String?
    var port: Int?
    var url: String?
}

public struct UserCoinModel: Codable {
    public var coin: Int?
    public var level: String?
}

public struct BoxContentModel: Codable {
    var awards: [AwardModel]?
    var id: Int?                // 奖品编号
    var playingDesc: String?    // 玩法说明
    public var signinStatus: Int?      // 签到状态(0:今日未签到，1:今日已签到，2:审核中)
    var times: Int?             // 累计签到次数
}

public struct AwardModel: Codable {
    var content: String?    // 内容
    var id: Int?            // 奖品编号
    var num: CGFloat?           // 数量
    public var succDesc: String?   // 成功签到文案
    var succRemark: String? // 成功签到说明
    var type: Int?          // 奖品类型(1:红包,2:游戏币,3:普通娃娃布料,4:稀有娃娃布料)
    var url: String?        // 图标地址
}

public struct UserInfoModel: Codable {
    public var coin: Int?
    public var signinStatus: Int?
    public var user: UserModel?
}

