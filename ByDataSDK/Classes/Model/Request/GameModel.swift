//
//  GameModel.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/12/3.
//

import UIKit

public struct GApplyModel: Codable {
    var optId: Int?             // 操作ID-上机成功才有返回
    var playTime: Int?          // 游戏时间-上机成功才有返回
    var isFreePlay: Int?        // 是否包机(免费币)-使用包机卡后，状态=1；非包机=0
    var freePlayCountDown: Int? // 包机倒计时-使用包机卡后，剩余包机时间；单位秒
    var data: DollModel?
}

public struct GCoinModel: Codable {
    var playTime: Int?
    var data: Int?
}

public struct GStatusModel: Codable {
    var busId: Int?                 // 机器ID
    var optId: Int?                 // 操作ID
    var uid: Int?                   // 在玩UID,若请求uid与在玩uid不一致则为0
    var playerUid: Int?             // 在玩UID
    var timeRemain: Int?            // 剩余游戏时间
    
    var autoFire: Int?              // 是否在自动开炮0否 1是
    
    var freeCoin: Int?              // 包机的免费币余额
    var freePlay: Int?              // 是否在使用包机卡0否 1是
    var freePlayCountDown: Int?     // 包机卡剩余时间
    
    var usePKCard: Int?             // 是否使用PK卡 0-否 1-是
    var pkCountDown: Int?           // PK倒计时
    var pkCoin: Int?                // PK余额
}

public struct GAllStatusModel: Codable {
    public var head: String?
    public var uid: Int?
    public var seat: Int?
    public var status: Int? // 0空闲 1占用 2维护中
}

public struct GSeatModel: Codable {
    public var seat: Int?
    public var busId: Int?
    public var price: Int?
    public var state: GAllStatusModel?
    
    init(seat: Int?) {
        self.seat = seat
    }
}
