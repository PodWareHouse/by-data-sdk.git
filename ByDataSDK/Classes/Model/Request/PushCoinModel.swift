
import UIKit

public struct PCApplyModel: Codable {
    var optId: Int?             // 操作ID-上机成功才有返回
    var playTime: Int?          // 游戏时间-上机成功才有返回
    var isFreePlay: Int?        // 是否包机(免费币)-使用包机卡后，状态=1；非包机=0
    var freePlayCountDown: Int? // 包机倒计时-使用包机卡后，剩余包机时间；单位秒
}

public struct PCPushModel: Codable {
    var optId: Int?             // 操作ID-上机成功才有返回
    var playTime: Int?          // 游戏时间-上机成功才有返回
    var data: PCCoinModel?
}

public struct PCCoinModel: Codable {
    var coin: Int?              // 游戏币余额
    var freeCoin: Int?          // 包机卡余额
}

public struct PCBoxValueModel: Codable {
    var isOpen: Int?            // 是否可打开1-可 0-否
    var total: Int?             // 宝箱总数值
    var value: Int?             // 宝箱当前数值
}

public struct PCJPModel: Codable {
    public var all: Int?               // 全盘
    public var jp1: Int?               // JP1
    public var jp2: Int?               // JP2
    public var jp3: Int?               // JP3
    public var player: [PCJPPlayerModel]?  // 联机上机的用户信息，非联机为空
}

public struct PCJPPlayerModel: Codable {
    public var name: String?           // 用户昵称
    public var head: String?           // 头像
    public var bus: String?            // 机器名
    
    public init(bus: Int) {
        self.bus = String(bus)
    }
}

public struct PCStatusModel: Codable {
    var data: StatusPlayModel?
    
    var result: Int?            // 状态, result=0空闲 result=2故障中 result=3霸机 100占用
//    var busStatus: Int?         // 状态,霸机情况下返回：300-（消耗游戏币霸机/包机卡霸机[不消耗游戏币]） 301-故障霸机
    
    var isFreePlay: Int?        // 是否使用包机卡 0-否 1-是
    var cardCountDown: Int?     // 包机卡倒计时
    
    var usePKCard: Int?         // 是否使用PK卡 0-否 1-是
    var pkCoin: Int?            // PK余额
    var pkCountDown: Int?       // PK倒计时
    
    var seatUid: Int?           // 霸机用户uid 只有霸机状态下才能返回
    var tips: String?           // 提示信息，当存在霸机信息，会返回

    var playerHeader: String?   // 当前玩家头像
    var playerName: String?     // 当前玩家昵称
    var playerVIP: String?      // 当前玩家VIP等级
}

public struct StatusPlayModel: Codable {
//    var busId: Int?         //
//    var uid: Int?           //
    var optId: Int?         //
    var gameTime: Int?      // 剩余游戏时间
    var playerUid: Int?     //
}

public struct PCOutRecordModel: Codable {
    var busId: Int?
    var optId: Int?
    var outCoin: Int?       // 得到的币
    var userCoin: Int?      // 用户余额
}
