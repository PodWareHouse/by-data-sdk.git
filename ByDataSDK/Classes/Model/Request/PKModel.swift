//
//  PKModel.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2022/12/14.
//

import Foundation

public struct PKListModel: Codable {
    public var play: [PKInfoModel]?    // 进行中的PK房间列表，注意进行中的PK房间列表会全部返回
    public var finish: [PKInfoModel]?  // 完成的PK房间列表，最大数据条数为10条；从第二页开始，play的数据为空；仅仅返回finish的数据
}

public struct PKInfoModel: Codable {
    public var busType: String?     // 游戏类型
    public var cardType: Int?       // 卡类型：1-初级(适用1-10倍房) 2-中级(适用11-50倍房) 3-高级(适用>50倍房)
    public var endTime: String?     // 最晚参与时间
    public var hasJoin: Int?        // 我是否参与该PK 0-否 1-是
    public var head: String?        // 发起用户头像信息
    public var maxUser: Int?        // PK参与人数 0-表示不限制人数
    public var name: String?        // 发起用户昵称信息
    public var needPassword: Int?   // 是否需要密码0-否 1-是
    public var official: Int?       // 是否官方活动0-否 1-是
    public var pkRoomId: Int?// pk房间ID
    public var reward: Int?         // 胜者预计金额
    public var rewardExtra: String? // 官方奖励说明，有可能是空
    public var roomPrice: Int?      // PK倍数
    public var startTime: String?   // 发起时间
    public var status: Int?         // 状态 1-进行中 2-已结束
    var uid: Int?            // 发起用户UID
    public var winner: Int?         // 获胜者uid
    public var winnerHead: String?  // 获胜者头像
    public var winnerName: String?  // 获胜者昵称
    public var winnerReward: Int?   // 胜者获得奖励的游戏币
    
    public var countdown: Int?   // 剩余时间；单位秒
    public var joinUser: [PKUserModel]?    // 参与人员信息
    var shareContent: String?        // 分享内容
    var shareTitle: String?        // 分享标题
    var shareUrl: String?        // 分享链接
}

public struct PKUserModel: Codable {
    public var head: String?        // 获胜者头像
    public var joinTime: String?    // 参与时间
    public var name: String?        // 获胜者昵称
    public var rank: Int?           // 排名
    public var result: Int?         // 成绩
    public var reward: Int?         // 获得奖励的金额
    public var status: Int?         // 参与状态 1-进行中 2-已结束
    var uid: Int?            // 获胜者uid
}

public struct PKCreateInfoModel: Codable {
    public var busType: [Int]?         // 可选择的机器类型：1-推币机 2-游戏机
    public var gamePrice: [Int]?       // 游戏机可选倍数
    public var highest: Int?           // 是否存在超级PK卡;0-不存在 1-存在
    public var highestPrice: [Int]?    // 超级PK卡的可选倍数范围
    public var initial: Int?           // 是否存在初级PK卡;0-不存在 1-存在
    public var initialPrice: [Int]?    // 初级PK卡的可选倍数范围
    public var maxJoin: [Int]?         // 可设置的最大参与人数 0-表示不限制
    public var middle: Int?            // 是否存在中级PK卡;0-不存在 1-存在
    public var middlePrice: [Int]?     // 中级PK卡的可选倍数范围
    public var pushPrice: [Int]?       // 推币机可选倍数
    public var senior: Int?            // 是否存在高级PK卡;0-不存在 1-存在
    public var seniorPrice: [Int]?     // 高级PK卡的可选倍数范围
}

public struct PKJoinModel: Codable {
    var data: DollModel?
    var dollBus: DollModel?
}

public struct PKModel: Codable {
    var data: DollModel?
    var dollBus: DollModel?
}

public struct PKRoomModel: Codable {
    public var cardType: Int?  // 卡类型：1-初级(适用1-10倍房) 2-中级(适用11-50倍房) 3-高级(适用>50倍房)
    public var maxJoin: [Int]? // 可设置的最大参与人数 0-表示不限制
}

public struct PKShareModel: Codable {
    var pkRoomId: Int?          // pk房间ID
    var uid: Int?               //
    var status: Int?            //
    var name: String?           // 发起用户昵称信息
    var startTime: String?      //
    var reward: Int?            //
    var cardType: Int?          // 卡类型：1-初级(适用1-10倍房) 2-中级(适用11-50倍房) 3-高级(适用>50倍房)
    var roomPrice: Int?         //
    var busType: String?        // 游戏类型
    var maxUser: Int?           // PK参与人数 0-表示不限制人数
    var hasJoin: Int?           // 我是否参与该PK 0-否 1-是
    var needPassword: Int?      // 是否需要密码0-否 1-是
    var official: Int?          // 是否官方活动0-否 1-是
    var countdown: Int?         // 剩余时间；单位秒
    var joinUser: [PKUserModel]?// 参与人员信息
    var downloadUrl: String?    // 下载链接
    var appName: String?        // app名称
}
