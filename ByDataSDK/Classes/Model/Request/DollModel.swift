
import UIKit

public struct DollClassifyModel: Codable {
    public var type: Int?
    public var name: String?
}

public struct DollModel: Codable {
    public var type: Int?              // 机器类型(0:娃娃机，1:推币机，2:游戏机，3:弹珠机)
    public var status: Int?            // 设备状态 0-空闲，1-游戏中,2-故障状态，
    public var busId: Int?             // 设备id
    public var name: String?           // 设备名称
    public var cover: String?   // 封面
    public var price: Int?             // 单价
    public var stream1: String?        // 正面流地址
    public var stream2Realy: String?   // 侧面流地址-操作者
    public var pushType: Int?          // 在type=1的时候使用 0欢乐马戏团 1超级马戏团
    public var jpData: Int?
    public var linkBus: Int?
    public var playMusic: Int?
    var seatUid: Int?           // 霸机用户uid
    public var seatRemark: String?     // 霸机提示信息 您已霸机：0分钟，霸机累计消耗：20游戏币
    var seat: Int?              // 位置
    public var seatType: String?
    public var seatMap: [String: [String: Int]]?
    public var stream3rtc: String?
    public var conversationId: String?
    public var rtc: Int?
    public var refreshBtn: Int?
    
}

public struct SeatSetModel: Codable {
    var busId: Int?     // 设备id
    var msg: String?
}

