
import UIKit

public struct CmsModel: Codable {
    var uid: Int?           // 聊天系统的用户ID
    public var gameUid: Int?       // 自己的uid
    var avatar: String?     // 用户头像
    var remark: String?     // 聊天系统的备注
    var name: String?       // 用户昵称
    var online: Int?        // 在线状态 0离线 1在线
    var userType: Int?      // 用户类型：1-普通用户 2-客服
}

public struct CmsFriendsModel: Codable {
    var uid: Int?               // 聊天系统的用户ID
    var friendUid: Int?         // 聊天系统的好友UID
    var lastMsgContent: String?  // 最近一条聊天消息
    public var unMsgCount: Int?        // 未读消息数量
    var modifiedTime: Int?      // 时间戳
    public var user: CmsModel?
}

public struct CmsChatModel: Codable, Hashable {
    var id: Int?
    public var uid: Int?           // 用户ID
    public var tid: Int?           // 用户ID;tid与uid作查询条件(uid存放小的id/tid存放大的id)
    public var senderUid: Int?     // 发送用户
    public var msgType: Int?       // 消息类型（0：普通文字消息，1：图片消息）
    public var content: String?     // 消息内容
    public var createTime: Int?    // 创建时间
    public var mediaHeight: Int?   // 媒体高度
    public var mediaWidth: Int?    // 媒体宽度
}

public struct CmsReceiveModel: Codable {
    
    public var type: Int?          // 消息类型
    public var subType: Int?       // 消息子类
    public var fromUid: Int?       // 发送者ID
    
    public var fromUser: CmsUser?  // 发送者
    public var attachment: Attachment? //
    
    public var toUid: Int?         // 接收用户ID
    public var content: String?    // 消息内容
    public var time: TimeInterval? // 发送时间
    
    public var unread: Int?    //
    
    init() {
        
    }
    
    // 消息已未读
    public mutating func setisread() {
        self.unread = 0
    }

    // 消息未读
    public mutating func setUnread() {
        self.unread = 1
    }
    
    public static func same(_ item: CmsReceiveModel, _ mo: CmsReceiveModel) -> Bool {
        if ((item.time == mo.time) &&
            (item.content == mo.content) &&
            (item.fromUid == mo.fromUid) &&
            (item.toUid == mo.toUid)) {
            return true
        }
        return false
    }
}

public struct CmsUser: Codable {
    var head: String?    // 用户头像
    public var name: String?    // 用户昵称
    public var uid: Int?        //用户ID
}

public struct Attachment: Codable {
    public  var targetId: Int?      // 目标ID，如充值项目具体的Code；跳转到某个房间的busId
    var title: String?      // 附件标题，H5连接使用
    public var h5Link: String?     // 打开的H5连接
    var mediaUrl: String?   // 图片连接/视频连接
    public var openType: Int?      // 打开浏览器方式 0/null app内打开；1 app外部打开
}

public struct BarrageModel: Codable {
    public var name: String?
    public var busId: String?
    public var coin: String?
    public var content: String?
    public var htmlStr: String?
    
    public init() {
        
    }
    
    public init(_ item: CmsReceiveModel) {
        self.htmlStr = item.content
        guard let attributedString = item.content?.html2AttributedString else {
            return
        }
        self.content = attributedString.string
        
        var ranges: [NSRange] = []
        attributedString.enumerateAttributes(in: NSRange(location: 0, length: attributedString.length),
                                             options: NSAttributedString.EnumerationOptions(rawValue: 0)) { dict, range, stopEnumerating in
            ranges.append(range)
        }
        if (ranges.count >= 2) {
            let range = ranges[1]
            self.name = attributedString.attributedSubstring(from: range).string
        }
        if (ranges.count >= 4) {
            let range = ranges[3]
            self.busId = attributedString.attributedSubstring(from: range).string
        }
        if (ranges.count >= 6) {
            let range = ranges[5]
            self.coin = attributedString.attributedSubstring(from: range).string
        }
    }
}

