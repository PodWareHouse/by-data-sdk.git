//
//  JsonModel.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2023/1/7.
//

import UIKit

public struct JsonModel: Codable {
    public var key: String?
    public var list: [String]?
    public var dic: [String: String]?
}

public struct MusicModel: Codable {
    public var bgmusic: Bool
    public var videomusic: Bool
    public var showbarrage: Bool
    
    public init() {
        self.bgmusic = true
        self.videomusic = true
        self.showbarrage = true
    }
}
