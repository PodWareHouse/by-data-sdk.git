//
//  ItemModel.swift
//  WaWaDuo
//
//  Created by seedmorn_gf on 2023/1/9.
//

import UIKit

public enum ItemType {
    case normal     // title、more
    case ison       // title、switch
    case image      // title、image
    case item       // title、image
    case customer       // title、image
    case system       // title、image
}

public class ItemModel: NSObject {
    
    public var type: ItemType = .normal
    public var title: String?
    public var image: String?
    public var selimage: String?
    public var issel: Bool = false
    public var state: Int?
    public var unread: Int?
    public var more: Bool = false
    public var row: Int = 0
    public var extend: String?

    public init(_ title: String?, _ image: String?, _ row: Int?) {
        self.title = title
        self.image = image
        self.row = row ?? 0
    }
    
    public init(type: ItemType, title: String?, issel: Bool?) {
        self.type = type
        self.title = title
        self.issel = issel ?? false
    }
    
    public init(type: ItemType, title: String?, more: Bool?) {
        self.type = type
        self.title = title
        self.more = more ?? false
    }
    
    public init(type: ItemType, title: String?, more: Bool?, extend: String?) {
        self.type = type
        self.title = title
        self.more = more ?? false
        self.extend = extend
    }
    
    public init(type: ItemType, title: String?, image: String?) {
        self.type = type
        self.title = title
        self.image = image
    }
    
    public init(type: ItemType, title: String?, image: String?, state: Int?) {
        self.type = type
        self.title = title
        self.image = image
        self.state = state
    }
    
    public init(image: String?, selimage: String?, state: Int, issel: Bool) {
        self.image = image
        self.selimage = selimage
        self.state = state
        self.issel = issel
    }
}
