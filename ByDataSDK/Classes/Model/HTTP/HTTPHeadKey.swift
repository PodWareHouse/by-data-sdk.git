
import UIKit

public struct HTTPHeadKey: Codable {
    
    let osType: Int      // 系统类别 0-android 1-ios
    public var imei: String     // 设备号
    var uid: Int         // 用户uid
    var loginKey: String // 登录校验码
    public let channel: String  // 渠道号
    public let version: Int     // 系统版本号
    let appCode: String  // app标识
    let lang: String // en tw cn
    
    public init(channel: String?, version: Int?, appCode: String?) {
        self.osType = 1
        self.imei = UIDevice.current.identifierForVendor?.uuidString ?? ""
        self.uid = 0
        self.loginKey = ""
        self.channel = channel ?? ""
        self.version = version ?? 0
        self.appCode = appCode ?? ""
        let lang = Bundle.main.preferredLocalizations.first
        print("lang = \(lang ?? "nil")")
        switch lang {
        case "zh-Hans-US","zh-Hans-CN","zh-Hans":
            self.lang = "cn"
        case "zh-TW","zh-HK":
            self.lang = "tw"
        default:
            self.lang = "en"
        }
    }
    
    public mutating func update(uid: Int?, loginKey: String?) {
        self.uid = uid ?? 0
        self.loginKey = loginKey ?? ""
    }
}

