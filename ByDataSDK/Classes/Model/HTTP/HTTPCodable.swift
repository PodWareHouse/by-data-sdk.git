
import Foundation

// MARK: - 一、连接地址
public struct HTTPModel: Hashable {
    public let host: String // 服务器域名地址
    let port: String // 端口
    
    public init(host: String, port: String) {
        self.host = host
        self.port = port
    }
}



// MARK: - 二、基础模型
public struct BaseCodable: Codable {
    var msg: String?    // 错误信息
    var result: Int?     // 请求状态码，一般通过state去判断
    var state: Bool?     // 请求状态 true-成功 false-失败
    
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
}



// MARK: - 三、data 模型
struct DataCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var data: Model?
    var playerHeader: String?
    var playerVIP: String?
    var playerName: String?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
}
struct DollCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var data: Model?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case data = "dollBus"
    }
}
struct InfoCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var data: Model?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case data = "info"
    }
}
struct AboutCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var data: Model?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case data = "aboutUs"
    }
}
struct CatalogsCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var data: Model?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case data = "catalogs"
    }
}
struct AwardInfoCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var data: Model?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case data = "awardInfo"
    }
}
struct DollBusCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var data: Model?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case data = "dollBus"
    }
}



// MARK: - 三、list 模型
struct ListCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var list: [Model]?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
}
struct DataListCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var list: [Model]?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case list = "data"
    }
}
struct ActivityListCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var list: [Model]?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case list = "activitiesList"
    }
}
struct ClassifyListCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var list: [Model]?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case list = "classifyList"
    }
}
struct BannerListCodable<Banner: Codable, Record: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var banners: [Banner]?
    var records: [Record]?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
}
struct WatchersListCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var list: [Model]?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case list = "watchers"
    }
}
struct CardListCodable<Model: Codable>: Codable {
    var msg: String?
    var result: Int?
    var state: Bool?
    var list: [Model]?
    init() {
        self.msg = ""
        self.result = 0
        self.state = false
    }
    enum CodingKeys: String, CodingKey {
        case msg
        case result
        case state
        case list = "cardList"
    }
}


