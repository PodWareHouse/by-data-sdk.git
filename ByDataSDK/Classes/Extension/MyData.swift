
import Foundation

public extension Data {
    /// 1、To Struct
    /// - Returns: struct
    func toCodable<T: Codable>() -> T? {
        let decoder = JSONDecoder()
        guard let decodedData = try? decoder.decode(T.self, from: self) else {
            print("toCodable - nil")
            return nil
        }
        return decodedData
    }
    
    /// 2、To Dictionary
    /// - Returns: Dictionary
    func toDictionary() -> [String: Any]?{
        do{
            let json = try JSONSerialization.jsonObject(with: self, options: .mutableContainers)
            let dic = json as! [String: Any]
            return dic
        }catch _ {
            return nil
        }
    }
}

