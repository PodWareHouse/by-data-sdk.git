
import Foundation

public extension Encodable {
    
    /// 1、Encodable 转 String
    /// - Returns: String
    func toJSONString() -> String {
        guard let data = try? JSONEncoder().encode(self)else{
            return "\(type(of: self)) 模型转 json字符串失败"
        }
        guard let str = String.init(data: data, encoding: .utf8) else {
            return "\(type(of: self)) 模型转 json字符串失败"
        }
        return str
    }
    
    /// 2、Encodable 转 Dictionary
    /// - Returns: Dictionary
    func toDictionary() -> [AnyHashable: Any]? {
        let str: String = self.toJSONString()
        guard let data = str.data(using: String.Encoding.utf8, allowLossyConversion: false) else {
            return nil
        }
        do{
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            let dic = json as! [AnyHashable: Any]
            return dic
        }catch _ {
            return nil
        }
    }
    
    /// 3、Encodable 转 Array
    /// - Returns:
    func toArray() -> [[AnyHashable: Any]]? {
        guard let data = try? JSONEncoder().encode(self) else {
            return nil
        }
        guard let arr = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [[AnyHashable: Any]] else {
            return nil
        }
        return arr
    }
}
