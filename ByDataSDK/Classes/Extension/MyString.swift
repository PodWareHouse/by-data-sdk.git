
import Foundation
import UIKit

public extension String {
    func isPassword() -> Bool {
        let refex : NSPredicate = NSPredicate(format: "SELF MATCHES %@", "^[\\x21-\\x7E]{6,12}$")
        if (refex.evaluate(with: self)) {
            return true
        }
        return false
    }
}

public extension String {
    func text() -> String {
        return JsonData.textDicJson(self)
    }
    
    func params() -> String {
        return JsonData.paramsDicJson(self)
    }
    
    func api() -> String {
        return JsonData.apiDicJson(self)
    }
    
    func info() -> String {
        return JsonData.infoDicJson(self)
    }
}

public extension String {
    
    /// 1、移除特殊字符
    /// - Parameter key: 特殊字符
    func removeKey(_ key: String?) -> String {
        guard (key != nil && !key!.isEmpty) else {
            return self
        }
        guard self.contains(key!) else {
            return self
        }
        return self.replacingOccurrences(of: key!, with: "")
    }
}

public extension String {
    
    /// 1、时间戳转成字符串
    /// - Parameters:
    ///   - time: 时间戳
    ///   - format: 时间格式
    /// - Returns: 字符串
    static func timeIntervalString(time: Double, format: String) -> String {
        let date:Date = Date.init(timeIntervalSince1970: time)
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date as Date)
    }
    

}

public extension String {
    
    /// 1、格式字符串转日期
    /// - Parameter format: 格式字符串
    /// - Returns: 日期
    func toDate(_ format: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
}

public extension String {
    
    /// 1、富文本设置
    /// - Parameters:
    ///   - font: 字体大小
    ///   - textColor: 字体颜色
    ///   - lineSpaceing: 行间距
    ///   - wordSpaceing: 字间距
    /// - Returns: 富文本
    func attributedString(font: UIFont, textColor: UIColor, lineSpaceing: CGFloat, wordSpaceing: CGFloat) -> NSAttributedString {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpaceing
        
        let attributed = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.paragraphStyle: style,
            NSAttributedString.Key.kern: wordSpaceing
        ] as [NSAttributedString.Key: Any]
        
        let att = NSMutableAttributedString(string: self, attributes: attributed)
        return att
    }
    
    /// 2、富文本设置
    /// - Parameters:
    ///   - strs: 字符串数组
    ///   - fonts: 字体数组
    ///   - textColors: 颜色数组
    /// - Returns: 富文本
    static func attributedString(strs: [String], fonts: [UIFont], textColors: [UIColor]) -> NSAttributedString {
        let text: String = strs.joined()
        let att = NSMutableAttributedString(string: text)
        for i in 0..<strs.count {
            let str = strs[i]
            let range: Range? = text.range(of: str)
            if (range != nil) {
                let location = text.distance(from: text.startIndex, to: range!.lowerBound)
                att.addAttribute(NSAttributedString.Key.font, value: fonts[i], range: NSRange(location: location, length: str.count))
                att.addAttribute(NSAttributedString.Key.foregroundColor, value: textColors[i], range: NSRange(location: location, length: str.count))
            }
        }
        return att
    }
    
    /// 3、html富文本
    /// - Returns: 富文本
    func attributedHtml() -> NSAttributedString? {
        guard let news = self.removingPercentEncoding, let data = news.data(using: .unicode) else {
            return nil
        }
        
        let option = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        guard let attStr = try? NSMutableAttributedString(data: data, options: option, documentAttributes: nil) else {
            return nil
        }
        
        let paraph = NSMutableParagraphStyle()
        paraph.lineSpacing = 4
        attStr.addAttributes([
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.paragraphStyle:paraph],
                              range: NSMakeRange(0, attStr.length))
        return attStr
    }
}

public extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                          options: [.documentType: NSAttributedString.DocumentType.html,
                                    .characterEncoding: String.Encoding.utf8.rawValue],
                          documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
