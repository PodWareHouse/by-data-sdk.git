
import Foundation

public extension Dictionary {
    
    /// 1、字典转Data
    /// - Returns: 返回Data
    func toData() -> Data? {
        if (!JSONSerialization.isValidJSONObject(self)) {
            print("Dictionary to Data - is not a valid object")
            return nil
        }
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: []) else {
            print("Dictionary to Data - nil")
            return nil
        }
        return data
    }
}
