import UIKit

public extension UIColor {
    // 1、十六进制转Color
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        let rgb = (
            R: Double((hex >> 16) & 0xff) / 255,
            G: Double((hex >> 08) & 0xff) / 255,
            B: Double((hex >> 00) & 0xff) / 255
        )
        self.init(red: rgb.R, green: rgb.G, blue: rgb.B, alpha: alpha)
    }
}
