
import UIKit
import Kingfisher

public extension UIImageView {
    func coverImage(_ link: String?) {
        self.kf.indicatorType = .activity
        self.kf.setImage(with: URL.init(string: link ?? ""))
    }
    
    func headImage(_ link: String?) {
        self.kf.indicatorType = .activity
        self.kf.setImage(with: URL.init(string: link ?? ""), placeholder: UIImage(named: "empty"))
    }
}
