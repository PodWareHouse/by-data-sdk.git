
import Foundation

public extension Array where Element : Hashable {
    // 1、利用Set特性进行去重（速度快，但无法保证数组的顺序）
    var nodup: [Element] {
        return Array(Set(self))
    }
    
}

public extension Array {
    func toData() -> Data? {
        if (!JSONSerialization.isValidJSONObject(self)) {
            return nil
        }
        let data = try? JSONSerialization.data(withJSONObject: self, options: [])
        return data
    }
}
