
import Foundation
import UIKit

public extension UIImage {
    
    /// 1、纯色图片
    /// - Parameters:
    ///   - size: 图片尺寸
    ///   - color: 图片颜色
    /// - Returns: 图片
    static func image(size: CGSize, color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

