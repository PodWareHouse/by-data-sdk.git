
import Foundation
import SafariServices

public extension UIViewController {
    /// 1、跳转网页
    /// - Parameters:
    ///   - link: 网址
    func presentToSafariServices(link: String) {
        let url: URL? = URL(string: link)
        let vc = SFSafariViewController(url: url!)
        self.present(vc, animated: true, completion: nil)
    }
    
    /// 2、跳转设置
    func presentToSetting() {
        guard let url = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}
