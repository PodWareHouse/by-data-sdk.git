
import Foundation

public extension Bundle {
    
    /// 1、读取pod库中的bundle
    /// - Parameter podName: pod库名
    /// - Returns: 库bundle
    static func bundleWithPod(podName: String) -> Bundle? {
        var bundlePath: String?
        
        for bundle: Bundle in Bundle.allBundles {
            bundlePath = bundle.path(forResource: podName, ofType: "bundle")
            if (bundlePath != nil) {
                let mainBundle: Bundle? = Bundle(path: bundlePath!)
                if (mainBundle != nil) {
                    return mainBundle!
                }
            }
        }
        
        for bundle: Bundle in Bundle.allFrameworks {
            bundlePath = bundle.path(forResource: podName, ofType: "bundle")
            if (bundlePath != nil) {
                let mainBundle: Bundle? = Bundle(path: bundlePath!)
                if (mainBundle != nil) {
                    return mainBundle!
                }
            }
        }
        return nil
    }
    
    /// 2、读取bundle数据
    /// - Parameter fileName: 文件名称
    /// - Parameter fileType: 文件类型（例：json）
    /// - Returns: Data数据
    func data(fileName: String, fileType: String) -> Data? {
        let path: String? = self.path(forResource: fileName, ofType: fileType)
        if (path != nil) {
            return try? Data(contentsOf: URL(fileURLWithPath: path!))
        }
        return nil
    }
    
    /// 3、读取pod中bundle资源的json文件
    /// - Parameters:
    ///   - podName: pod库名
    ///   - fileName: 文件名称
    /// - Returns: JsonModel
    static func jsonModel(podName: String, fileName: String) -> JsonModel? {
        let bundle: Bundle? = Bundle.bundleWithPod(podName: podName)
        let data: Data? = bundle?.data(fileName: fileName, fileType: "json")
        return data?.toCodable()
    }
    
    /// 4、读取pod中bundle资源的mp3文件
    /// - Parameters:
    ///   - podName: pod库名
    ///   - fileName: 文件名称
    /// - Returns: 文件路径
    static func musicPath(podName: String, fileName: String) -> String? {
        let bundle: Bundle? = Bundle.bundleWithPod(podName: podName)
        return bundle?.path(forResource: fileName, ofType: "mp3")
    }
}

public extension Bundle {
    var appVersion: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    
    var appBuild: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
    
    var appName: String {
        return infoDictionary?["CFBundleDisplayName"] as? String ?? ""
    }
}
 


