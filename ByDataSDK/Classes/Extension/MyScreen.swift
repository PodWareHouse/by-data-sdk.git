
import UIKit

public extension UIScreen {
    static var width: CGFloat = UIScreen.main.bounds.width
    static var height: CGFloat = UIScreen.main.bounds.height
    static let tabbar: CGFloat = 49
    static let navHeight: CGFloat = 44 + safeAreaTop
    static var safeAreaBottom: CGFloat {
        if #available(iOS 13.0, *) {
            return keyWindow?.safeAreaInsets.bottom ?? 0
        } else if #available(iOS 11.0, *) {
            let inset = UIApplication.shared.delegate?.window??.safeAreaInsets
            return inset?.bottom ?? 0
        } else {
            return 0
        }
    }
    static var safeAreaTop: CGFloat {
        if #available(iOS 13.0, *) {
            return keyWindow?.safeAreaInsets.top ?? 0
        } else if #available(iOS 11.0, *) {
            let inset = UIApplication.shared.delegate?.window??.safeAreaInsets
            return inset?.top ?? 20
        } else {
            return 20
        }
    }
    static var keyWindow: UIWindow? {
        if #available(iOS 14.0, *) {
            if let window = UIApplication.shared.connectedScenes.map({$0 as? UIWindowScene}).compactMap({$0}).first?.windows.first {
                return window
            } else if let window = UIApplication.shared.delegate?.window {
                return window
            } else {
                return nil
            }
        } else if #available(iOS 13.0, *) {
            if let window = UIApplication.shared.connectedScenes.filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first {
                return window
            } else if let window = UIApplication.shared.delegate?.window {
                return window
            } else {
                return nil
            }
            
        } else {
            if let window = UIApplication.shared.delegate?.window {
                return window
            } else {
                return nil
            }
        }
    }
}
