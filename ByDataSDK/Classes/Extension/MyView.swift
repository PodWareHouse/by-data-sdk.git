
import Foundation
import UIKit

public extension UIView {
    
    /// 1、获取路径
    /// - Parameters:
    ///   - list: 角标数组[.topRight, .topLeft]
    ///   - radius: 圆角宽度
    /// - Returns: 路径
    private func path(_ list: [UIRectCorner], _ radius: CGFloat) -> UIBezierPath {
        if (list.isEmpty) {
            return UIBezierPath(roundedRect: self.bounds, cornerRadius: radius)
        }
        var value: UInt?
        for item in list {
            if (value == nil) {
                value = item.rawValue
            } else {
                value = (value! | item.rawValue)
            }
        }
        let corner = UIRectCorner(rawValue: value!)
        return UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corner, cornerRadii: CGSize(width: radius, height: radius))
    }
    
    /// 2、设置遮照
    /// - Parameter path: 路径
    private func setCorner(path: UIBezierPath) {
        let layer = CAShapeLayer()
        layer.frame = self.bounds
        layer.path = path.cgPath
        self.layer.mask = layer;
    }
    
    func corner(_ radius: CGFloat) {
        self.setCorner(path: self.path([], radius))
    }

    func circle() {
        var radius: CGFloat = self.bounds.size.height / 2.0
        if (self.bounds.size.height > self.bounds.size.width) {
            radius = self.bounds.size.width / 2.0
        }
        self.corner(radius)
    }
    
    func pathCorner(_ list: [UIRectCorner], _ radius: CGFloat) {
        self.setCorner(path: self.path(list, radius))
    }
    
    func topCorner(_ radius: CGFloat) {
        self.pathCorner([.topLeft, .topRight], radius)
    }

    func topLeftCorner(_ radius: CGFloat) {
        self.pathCorner([.topLeft], radius)
    }

    func bottomCorner(_ radius: CGFloat) {
        self.pathCorner([.bottomLeft, .bottomRight], radius)
    }

    func bottomRightCorner(_ radius: CGFloat) {
        self.pathCorner([.bottomRight], radius)
    }

    func leftCorner(_ radius: CGFloat) {
        self.pathCorner([.topLeft, .bottomLeft], radius)
    }
}

public extension UIView {
    /// 1、设置边框圆角
    /// - Parameters:
    ///   - path: 路径
    ///   - color: 颜色
    ///   - lineWidth: 边框宽度
    private func setBorderCorner(_ path: UIBezierPath, _ color: UIColor?, _ lineWidth: CGFloat) {
        let layer = CAShapeLayer()
        layer.frame = self.bounds
        layer.path = path.cgPath
        layer.lineWidth = lineWidth
        if (color != nil) {
            layer.strokeColor = color!.cgColor
        } else {
            layer.strokeColor = UIColor.black.cgColor
        }
        layer.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(layer)
        
    }
    
    func border(_ color: UIColor?, _ width: CGFloat) {
        let path = self.path([], 0)
        self.setBorderCorner(path, color, width)
    }
    
    func borderCorner(_ color: UIColor?, _ radius: CGFloat, _ width: CGFloat) {
        let path = self.path([], radius)
        self.setCorner(path: path)
        
        let mpath = self.path([], radius)
        self.setBorderCorner(mpath, color, width)
    }
    
    func pathBoardCorner(_ list: [UIRectCorner], _ color: UIColor?, _ radius: CGFloat, _ width: CGFloat) {
        let path = self.path(list, radius)
        self.setCorner(path: path)
        
        let mpath = self.path(list, radius)
        self.setBorderCorner(mpath, color, width)
    }
    
}

public extension UIView {
    func setShadow(sColor:UIColor, offset:CGSize, opacity:Float, radius:CGFloat, corner: CGFloat) {
        //设置阴影颜色
        self.layer.shadowColor = sColor.cgColor
        //设置透明度
        self.layer.shadowOpacity = opacity
        //设置阴影半径
        self.layer.shadowRadius = radius
        //设置阴影偏移量
        self.layer.shadowOffset = offset
        
        if (corner > 0) {
            self.layer.cornerRadius = corner
        }
    }
    
    func grayTextShadow(corner: CGFloat, color: UIColor?) {
        self.setShadow(sColor: color ?? UIColor.black, offset: CGSize(width: 0, height: 0), opacity: 1, radius: 5, corner: corner)
    }
}

public extension UIView {
    
    /// 1、是否显示在屏幕上
    /// - Returns: true->显示在屏幕, false->不显示在屏幕上
    func isDisplayedInScreen() -> Bool {
        let screenRect = UIScreen.main.bounds
        
        let rect = self.convert(self.frame, from: nil)
        if (rect.isEmpty || rect.isNull) {
            return false
        }
        
        if (self.isHidden) {
            return false
        }
        
        if (self.superview == nil) {
            return false
        }
        
        if (__CGSizeEqualToSize(rect.size, CGSize.zero)) {
            return false
        }
        
        let intersectionRect = rect.intersection(screenRect)
        if (intersectionRect.isEmpty || intersectionRect.isNull) {
            return false
        }
        
        return true
    }
    
}

public extension UIView{
    //MARK:- 绘制虚线
    func drawDashLine(strokeColor: UIColor, lineWidth: CGFloat = 1, lineLength: Int = 10, lineSpacing: Int = 5) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.bounds = self.bounds
        shapeLayer.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPhase = 0 //从哪个位置开始
        //每一段虚线长度 和 每两段虚线之间的间隔
        shapeLayer.lineDashPattern = [NSNumber(value: lineLength), NSNumber(value: lineSpacing)]
        let path = CGMutablePath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.layer.bounds.width, y: 0))
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
}
