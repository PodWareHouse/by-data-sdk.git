
import Foundation

public extension Date {
    
    /// 1、Date转字符串
    /// - Parameter format: 字符串格式
    /// - Returns: 时间字符串
    func toString(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    /// 2、日期转格式字符串
    /// - Returns: 格式字符串
    func titleHeader() -> String {
        if (self.istoday()) {
            return self.toString("yyyy-MM-dd") + "（" + "other_11".text() + "）"
        } else if (self.isyesterday()) {
            return self.toString("yyyy-MM-dd") + "（" + "other_16".text() + "）"
        } else if (self.isWeek(Date())) {
            let day = self.weekIndex()
            let arr = ["一","二","三","四","五","六","日"]
            return self.toString("yyyy-MM-dd") + "（\("other_17".text())\(arr[day])）"
        } else {
            return self.toString("yyyy-MM-dd")
        }
    }
    
    /// 3、日期转格式字符串
    /// - Returns: 格式字符串
    func titleChatHeader() -> String {
        if (self.istoday()) {
            return self.toString("\("other_11".text()) HH:mm")
        } else if (self.isyesterday()) {
            return self.toString("\("other_16".text()) HH:mm")
        } else if (self.isWeek(Date())) {
            let day = self.weekIndex()
            let arr = ["一","二","三","四","五","六","日"]
            return self.toString("\("other_17".text())\(arr[day]) HH:mm")
        } else {
            return self.toString("yyyy-MM-dd HH:mm")
        }
    }
}

public extension Date {
    func istoday() -> Bool{
        let calendar = Calendar.current
        let unit: Set<Calendar.Component> = [.day,.month,.year]
        let nowComps = calendar.dateComponents(unit, from: Date())
        let compon = calendar.dateComponents(unit, from: self)
        
        return (compon.year == nowComps.year) &&
        (compon.month == nowComps.month) &&
        (compon.day == nowComps.day)
    }
    
    func isyesterday() -> Bool {
        let calendar = Calendar.current
        let unit: Set<Calendar.Component> = [.day,.month,.year]
        let nowComps = calendar.dateComponents(unit, from: Date())
        let compon = calendar.dateComponents(unit, from: self)
        if compon.day == nil || nowComps.day == nil {
            return false
        }
        let count = nowComps.day! - compon.day!
        return (compon.year == nowComps.year) &&
            (compon.month == nowComps.month) &&
            (count == 1)
    }
    
    func istomorrow() -> Bool {
        let calendar = Calendar.current
        let unit: Set<Calendar.Component> = [.day,.month,.year]
        let nowComps = calendar.dateComponents(unit, from: Date())
        let compon = calendar.dateComponents(unit, from: self)
        if compon.day == nil || nowComps.day == nil {
            return false
        }
        let count = nowComps.day! - compon.day!
        return (compon.year == nowComps.year) &&
            (compon.month == nowComps.month) &&
            (count == -1)
    }
    
    func isWeek(_ last: Date) -> Bool {
        let comps_1 = Calendar.current.dateComponents([.day], from: self, to: last)
        let differ = abs(comps_1.day!)
        let compare = Calendar.current.compare(self, to: last, toGranularity: Calendar.Component.day)
        
        var lastDate: Date
        if compare == ComparisonResult.orderedAscending {
            lastDate = self
        }else {
            lastDate = last
        }
        
        let index = lastDate.weekIndex()
        
        let value = differ + index
        return value > 7 ? false : true
    }
    
    func weekIndex() -> Int {
        let comps = Calendar.current.dateComponents([.weekday], from: self)
        return (comps.weekday! - 1) == 0 ? 7 : (comps.weekday! - 1)
    }
    
    func isYear(_ last: Date) -> Bool {
        let calendar = Calendar.current
        let unit: Set<Calendar.Component> = [.day,.month,.year]
        let nowComps = calendar.dateComponents(unit, from: Date())
        let compon = calendar.dateComponents(unit, from: self)
        
        return (compon.year == nowComps.year)
    }
}
